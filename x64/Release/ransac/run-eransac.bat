set "FITEXE=C:/Users/dli/Desktop/Codes/ERANSAC/x64/Release/ransac/ERansac.exe"
set "SCANDIR=\\merlfs1.merl.com\Projects\SA\LearningOnPointClouds\Dataset\Blensor\TEST-20\data"
set "RESDIR=\\merlfs1.merl.com\Projects\SA\LearningOnPointClouds\Dataset\Blensor\TEST-20\eRansac"
set "FITCONFIG=ransac.config"
set "RESSUFFFIX=-fit-1000-0.03-30-45-0.3"

FOR /R %SCANDIR% %%G in (.) DO (
    if not exist %RESDIR%\\%%~nG%RESSUFFFIX% mkdir %RESDIR%\\%%~nG%RESSUFFFIX%
    xcopy /s/y %FITCONFIG% %RESDIR%\\%%~nG%RESSUFFFIX%
    %FITEXE% %FITCONFIG% %SCANDIR%\\%%~nG %RESDIR%\\%%~nG%RESSUFFFIX% 0.3
)