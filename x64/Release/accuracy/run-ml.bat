set "FITEXE=C:/Users/dli/Desktop/Codes/ERANSAC/x64/Release/accuracy/ERansac.exe"
set "SCANDIR=\\merlfs1.merl.com\Projects\SA\LearningOnPointClouds\Dataset\Blensor\vali-20\data"
set "RESDIR=\\merlfs1.merl.com\Projects\SA\LearningOnPointClouds\Dataset\Blensor\vali-20\result\ENO_ML_lr1e-1_poly_iter_12k"
set "FITCONFIG=ml.config"
set "RESSUFFFIX=-fit"

FOR /R %SCANDIR% %%G in (.) DO (
    if not exist %RESDIR%\\%%~nG%RESSUFFFIX% mkdir %RESDIR%\\%%~nG%RESSUFFFIX%
    xcopy /s/y %FITCONFIG% %RESDIR%\\%%~nG%RESSUFFFIX%
    %FITEXE% %FITCONFIG% %SCANDIR%\\%%~nG %RESDIR%\\%%~nG %RESDIR%\\%%~nG%RESSUFFFIX% 0.5
)