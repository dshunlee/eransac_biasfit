set "FITEXE=ERansac.exe"
set "SCANDIR=C:\Users\duanshun\Downloads\TEST-20"
set "RESDIR=C:\Users\duanshun\Downloads\eval_output"
set "OUTDIR=C:\Users\duanshun\Downloads\eval_output_fit"
set "FITCONFIG=fit.config"
set "RESSUFFFIX=-fit-1000-0.03-30-45-0.3"

FOR /R %SCANDIR% %%G in (.) DO (
    if not exist %OUTDIR%\\%%~nG%RESSUFFFIX% mkdir %OUTDIR%\\%%~nG%RESSUFFFIX%
    xcopy /s/y %FITCONFIG% %OUTDIR%\\%%~nG%RESSUFFFIX%
    %FITEXE% %FITCONFIG% %SCANDIR%\\%%~nG %RESDIR%\\%%~nG %OUTDIR%\\%%~nG%RESSUFFFIX% 0.3
)