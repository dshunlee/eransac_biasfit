'''
save compressed npz file to uncompressed npz file

the scripts uncompress all npz files in the folder and keeps the subfolder structure
'''


import os
import numpy as np


def get_file(folder, extension):
    res = []
    for root, dirs, files in os.walk(folder):
        for file in files:
            if file.endswith(extension):
                res.append(os.path.join(root, file))
    return res

def uncompress(orgfolder,outfolder, crop):
    files = get_file(orgfolder,'.npz')
    for fi in files:
        data = np.load(fi)
        newf = fi.replace(orgfolder,outfolder)
        if not os.path.exists(os.path.dirname(newf)):
            os.makedirs(os.path.dirname(newf), exist_ok=True)

        out = {}
        for key in data.keys():
            if crop and data[key].shape[0] == 640:
                out[key] = data[key][80:-80,:,:]
            else:
                out[key] = data[key]
        np.savez(newf,**out)



if __name__ == '__main__':
    import argparse

    # Parse command line arguments
    parser = argparse.ArgumentParser(
        description='Convert compressed npz to uncompressed npz.')
    parser.add_argument('--comp', required=True,
                        metavar="path/to/compressed/npz/folder",
                        help='path/to/compressed/npz/folder')
    parser.add_argument('--out', required=True,
                        metavar="/path/to/output/npz/folder",
                        help='path/to/output/npz/folder')
    parser.add_argument('--crop', required=False,
                        default = False,
                        metavar="Crop the result to 480 x 640 x n, (True or False)",
                        help='Crop the result to 480 x 640 x n,, (True or False)',
                        type=bool)
    args = parser.parse_args()

    uncompress(args.comp,args.out,args.crop)

