#pragma once

#include "cnpycloud.h"
#include "cnpy.h"

#include <fstream>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <exception>

namespace CloudReader {

	static std::string int2strw(int id, int w) {
		std::stringstream ss;
		ss << std::setw(w) << std::setfill('0') << id;
		return ss.str();
	}

#ifndef cnpyisnan
#define cnpyisnan(x,y,z) (((x==0) && (y==0) && (z==0)) || ((x!=x) && (y!=y) && (z!=z)))
#endif 

	int read_xyz(std::string fname, PointCloud& pcl) {
		int N = 0;
		std::ifstream in(fname);
		std::string unused;
		while (!in.eof()) {
			std::getline(in, unused);
			if (!unused.empty())
				N++;
		}
		in.clear();
		in.seekg(0, std::ios::beg);
		pcl.resize(N);

		for (int pi = 0;pi < N;pi++) {
			Point pt;
			in >> pt.pos[0] >> pt.pos[1] >> pt.pos[2] >> pt.normal[0] >> pt.normal[1] >> pt.normal[2];

			//pt.attr[0]

			pcl.push_back(pt);
		}

		return N;
	}


	int read_npz(std::string fname, PointCloud& pcl) {
		float *xyz = 0, *normal = 0, *probs = 0;
		//int64_t *cls=0, *ins=0;
		int Npts = 0;
		pcl.n_probs = 0;
		try {
			cnpy::npz_t scan = cnpy::npz_load(fname);
			for (cnpy::npz_t::iterator it = scan.begin();it != scan.end();it++) {
				if (it->first == "data")
					xyz = reinterpret_cast<float*>(scan["data"].data);
				if (it->first == "normal_est") {
					normal = reinterpret_cast<float*>(scan["normal_est"].data);
					pcl.has_normal = true;
					pcl.use_normal_est = true;
				}
				if (normal == 0 && it->first == "normal") {
					normal = reinterpret_cast<float*>(scan["normal"].data);
					pcl.use_normal_est = false;
				}
				//if (it->first == "ins") {
				//	ins = reinterpret_cast<int64_t*>(scan["ins"].data);
				//	pcl.has_ins;
				//}
				//if (it->first == "cls") {
				//	cls = reinterpret_cast<int64_t*>(scan["cls"].data);
				//	pcl.has_clas = true;
				//}
			}
			if (xyz == 0 || normal == 0)
				throw(std::invalid_argument("The file does not have point coordinates or normal!"));
			Npts = 1;
			for (int i = 1;i < scan["data"].shape.size();i++)
				Npts *= scan["data"].shape[i];

			std::vector<int> steps(3, 0);
			for (int i = 1;i < 3;i++) steps[i] = steps[i - 1] + Npts;

			pcl.reserve(Npts);

			float x, y, z;
			for (int i = 0;i < Npts;i++, xyz++, normal++) {
				x = *xyz; y = *(xyz + steps[1]); z = *(xyz + steps[2]);
				if (!cnpyisnan(x, y, z)) {
					Point p(i, Vec3f(x, y, z), Vec3f(*normal, *(normal + steps[1]), *(normal + steps[2])));
					//if (cls) p.addClass(*cls++);
					//if (ins) p.addInstance(*ins++);
					pcl.push_back(p);
				}
				//else {
				//	if (cls) cls++;
				//	if (ins) ins++;
				//}
			}
			scan.destruct();
			//pcl.resize(pcl.size());
			return pcl.size();
		}
		catch (std::exception& e) {
			std::cerr << e.what() << std::endl;
			return 0;
		}
		return Npts;
	}
	int read_data_label(std::string datfn, std::string labfn, PointCloud& pcl) {
		float *xyz = 0, *normal = 0, *probs = 0;
		//int8_t *cls = 0, *ins = 0;
		int Npts = 0;
		try {
			// find essential data
			cnpy::npz_t scan = cnpy::npz_load(datfn);
			if (scan.find("data") == scan.end()) {
				throw(std::invalid_argument("No dataset named \"data\" found in the file!"));
			}
			if (scan.find("normal") == scan.end() && scan.find("normal_est") == scan.end()) {
				throw(std::invalid_argument("No dataset named \"normal\" or \"normal_est\" found in the file!"));
			}

			Npts = 1;
			for (int i = 1;i < scan["data"].shape.size();i++)
				Npts *= scan["data"].shape[i];

			// load data
			for (cnpy::npz_t::iterator it = scan.begin();it != scan.end();it++) {
				if (it->first == "data")
					xyz = reinterpret_cast<float*>(scan["data"].data);
				if (it->first == "normal_est") {
					normal = reinterpret_cast<float*>(scan["normal_est"].data);
					pcl.has_normal = true;
					pcl.use_normal_est = true;
				}
				if (normal == 0 && it->first == "normal") {
					normal = reinterpret_cast<float*>(scan["normal"].data);
					pcl.use_normal_est = false;
				}
				//if (it->first == "ins") {
				//	ins = reinterpret_cast<int8_t*>(scan["ins"].data);
				//	pcl.has_ins;
				//}
				//if (it->first == "cls") {
				//	cls = reinterpret_cast<int8_t*>(scan["cls"].data);
				//	pcl.has_clas = true;
				//}
			}

			cnpy::npz_t label = cnpy::npz_load(labfn);
			if (label.find("prob") == label.end()) {
				throw(std::invalid_argument("No dataset named \"prob\" found in the file!"));
			}
			else {
				int Nlabel = 1;
				for (int i = 1;i < label["prob"].shape.size();i++)
					Nlabel *= label["prob"].shape[i];

				assert(label["prob"].shape[0] >= 4); // currently handle only first 4 class
				assert(Nlabel == Npts); // has the same size

				probs = reinterpret_cast<float*>(label["prob"].data);
				pcl.has_prob = true;
			}

			// load label
			pcl.n_probs = label["prob"].shape[0];
			probs = reinterpret_cast<float*>(label["prob"].data);
			std::vector<int> steps(label["prob"].shape[0], 0); // currently, handle only 4 classes 
			for (int i = 1;i < label["prob"].shape[0];i++) steps[i] = steps[i - 1] + Npts;

			pcl.reserve(Npts);
			float x, y, z;
			for (int i = 0;i < Npts;i++, xyz++, normal++,probs++) {
				x = *xyz; y = *(xyz + steps[1]); z = *(xyz + steps[2]);
				if (!cnpyisnan(x, y, z)) {
					Point p(i, Vec3f(x, y, z), Vec3f(*normal, *(normal + steps[1]), *(normal + steps[2])));
					//if (cls) p.addClass(*cls++);
					//if (ins) p.addInstance(*ins++);
					if (pcl.n_probs >= 4) 
						p.addProbability(Vec6df(*probs, *(probs + steps[1]), *(probs + steps[2]), *(probs + steps[3])));
					if (pcl.n_probs >= 5) 
						p.probs[4] = *(probs + steps[4]);
					if (pcl.n_probs >= 6)
						p.probs[5] = *(probs + steps[5]);

					pcl.push_back(p);
				}
			}
			scan.destruct();
			label.destruct();
			return pcl.size();
		}
		catch (std::exception& e) {
			std::cerr << e.what() << std::endl;
			return 0;
		}
		return Npts;
	}

	int read_data_label_maskrcnn(std::string datfn, std::string labfn, PointCloud& pcl) {
		float *xyz = 0, *normal = 0, *scores = 0;
		unsigned char *masks = 0;
		unsigned int *pre_cls = 0;
		//int8_t *cls = 0, *ins = 0;
		int Npts = 0;
		try {
			// find essential data
			cnpy::npz_t scan = cnpy::npz_load(datfn);
			if (scan.find("data") == scan.end()) {
				throw(std::invalid_argument("No dataset named \"data\" found in the file!"));
			}
			if (scan.find("normal") == scan.end() && scan.find("normal_est") == scan.end()) {
				throw(std::invalid_argument("No dataset named \"normal\" or \"normal_est\" found in the file!"));
			}

			Npts = 1;
			for (int i = 1; i < scan["data"].shape.size(); i++)
				Npts *= scan["data"].shape[i];

			// load data
			for (cnpy::npz_t::iterator it = scan.begin(); it != scan.end(); it++) {
				if (it->first == "data")
					xyz = reinterpret_cast<float*>(scan["data"].data);
				if (it->first == "normal_est") {
					normal = reinterpret_cast<float*>(scan["normal_est"].data);
					pcl.has_normal = true;
					pcl.use_normal_est = true;
				}
				if (normal == 0 && it->first == "normal") {
					normal = reinterpret_cast<float*>(scan["normal"].data);
					pcl.use_normal_est = false;
				}
				//if (it->first == "ins") {
				//	ins = reinterpret_cast<int8_t*>(scan["ins"].data);
				//	pcl.has_ins;
				//}
				//if (it->first == "cls") {
				//	cls = reinterpret_cast<int8_t*>(scan["cls"].data);
				//	pcl.has_clas = true;
				//}
			}

			cnpy::npz_t label = cnpy::npz_load(labfn);
			if (label.find("mask") == label.end()) {
				throw(std::invalid_argument("No dataset named \"mask\" found in the file!"));
			}
			else if (label.find("cls") == label.end()) {
				throw(std::invalid_argument("No dataset named \"cls\" found in the file!"));
			}
			else if (label.find("score") == label.end()) {
				throw(std::invalid_argument("No dataset named \"score\" found in the file!"));
			}
			else {
				pre_cls = reinterpret_cast<unsigned int*>(label["cls"].data);
				scores = reinterpret_cast<float*>(label["score"].data);

				int Nlabel = 1;
				for (int i = 0; i < label["mask"].shape.size()-1; i++)
					Nlabel *= label["mask"].shape[i];
				assert(Nlabel == Npts); // has the same size

				masks = reinterpret_cast<unsigned char *>(label["mask"].data);
				pcl.has_mask = true;
			}

			// load maskrcnn label
			pcl.n_masks = label["mask"].shape[2];
			pcl.n_maskrcnn_cnt.resize(pcl.n_masks, 0);
			pcl.n_object_idx.resize(pcl.n_masks);
			pcl.n_maskrcnn_score.insert(pcl.n_maskrcnn_score.end(), scores, scores + pcl.n_masks);
			pcl.n_maskrcnn_clas.insert(pcl.n_maskrcnn_clas.end(), pre_cls, pre_cls + pcl.n_masks);
			masks = reinterpret_cast<unsigned char*>(label["mask"].data);
			std::vector<int> steps(scan["data"].shape[0], 0); // currently, handle only 4 classes 
			for (int i = 1; i < scan["data"].shape[0]; i++) steps[i] = steps[i - 1] + Npts;
			
			pcl.reserve(Npts);
			float x, y, z;
			int ptid = 0;
			for (int i = 0; i < Npts; i++, xyz++, normal++) {
				x = *xyz; y = *(xyz + steps[1]); z = *(xyz + steps[2]);
				if (!cnpyisnan(x, y, z)) {
					Point p(i, Vec3f(x, y, z), Vec3f(*normal, *(normal + steps[1]), *(normal + steps[2])));
					//if (cls) p.addClass(*cls++);
					//if (ins) p.addInstance(*ins++);
					for (int k = 0; k < pcl.n_masks; k++, masks++)
						if (*masks) {
							pcl.n_maskrcnn_cnt[k] ++;
							pcl.n_object_idx[k].push_back(ptid);
						}

					pcl.push_back(p);

					ptid++;
				}
				else
					masks+=pcl.n_masks;
			}
			scan.destruct();
			label.destruct();
			/* print all instances
			for (int i = 0; i < pcl.n_masks; i++) {
				std::ofstream out(std::to_string(i) + ".xyz");
				for (int j = 0; j < pcl.n_maskrcnn_cnt[i]; j++) {
					int pid = pcl.n_object_idx[i][j];
					out << pcl[pid].pos[0] << " " << pcl[pid].pos[1] << " " << pcl[pid].pos[2] << std::endl;
				}
				out.close();
			}

			return pcl.size();
			*/
		}
		catch (std::exception& e) {
			std::cerr << e.what() << std::endl;
			return 0;
		}
		return Npts;
	}

	bool fexists(const char *filename) {
		std::ifstream ifile(filename);
		return (bool)ifile;
	}

};
