#pragma once

#include <string>
#include <map>
#include "primfit.h"

class ProbFit: public PrimFit{
public:
	ProbFit(){}
	// fitting after classification with probability map
	void fit(std::vector<Options > pbfopt, bool globcorr=true);
	// fitting use naive efficient ransac
	void fitall(std::vector<Options > pbfopt); 
	// final optimization with pcl (requires pcl library, set PCLOPTIMIZATION if needed)
	void optimize();
	// save primitives to file
	void savePrimitives(std::string prefix);
protected:
	int fitPrimitive(Options pbfopt, bool globcorr);// fit primitive type primi with constructor primcon
	int threshSelect(int primi, double thresh, PointCloud& pcli);// select remaining points for primitive type primi
	int instanceSelect(int primi, double pthresh, std::vector<PointCloud>& pcli,double ethresh=0.5);// segment by instances
	int maxProbSelect(int primi, PointCloud& pcli);// select points with maximum probability
	int maxProbWithThreshSelect(int primi, double thersh, PointCloud& pcli);// select points with maximum probability & thresh
	int top2ProbSelect(int primi, PointCloud& pcli);// select points with top2 probability
	bool calLabel(PointCloud& cloud, MiscLib::Vector< DetectedShape >& shapes);
	bool calLabel(std::vector<std::vector<int> >& gindex, int primi);
};
