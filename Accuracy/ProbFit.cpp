#include "ProbFit.h"
#include "cnpycloud.h"
#include "primitiveio.h"
#include <stdio.h>

#define QUIET
//#define BIASSFIT_OBJECTSEG

#ifdef BIASSFIT_OBJECTSEG
#include "connectedcomps.h"
#endif 

#ifdef PCLOPTIMIZATION
#include "pcloptimize.h"
#include <pcl/io/pcd_io.h>
#endif

void ProbFit::fit(std::vector<Options> pbfopt, bool globcorr) {
	
	for (int i = 0;i < pbfopt.size();i++) {

		int nprim = fitPrimitive(pbfopt[i],globcorr);
		std::cout << "Found " << nprim << " instance of " << "prim: " << pbfopt[i].primID << std::endl;

	}
}
void ProbFit::fitall(std::vector<Options> pbfopt) { 

	RansacShapeDetector detector(pbfopt[0]);
	for (int i = 0;i < pbfopt.size();i++)
	{
		switch (pbfopt[i].primID) {
		case(0):
			detector.Add(new PlanePrimitiveShapeConstructor());
			break;
		case(1):
			detector.Add(new SpherePrimitiveShapeConstructor());
			break;
		case(2):
			detector.Add(new CylinderPrimitiveShapeConstructor());
			break;
		case(3):
			detector.Add(new ConePrimitiveShapeConstructor());
			break;
		default:
			break;
		}
	}

	detector.Detect(pcl, 0, pcl.size(), &shapes);
	calLabel(pcl, shapes);

}

void ProbFit::optimize() {
#ifdef PCLOPTIMIZATION
	pcl.genPix2IDMap();

	std::map<int, pcl::SacModel> sacmodmap = { {0, pcl::SACMODEL_NORMAL_PLANE}, {1, pcl::SACMODEL_NORMAL_SPHERE},
	{2,pcl::SACMODEL_CYLINDER},{3,pcl::SACMODEL_CONE} };

	PCLPrimFit pclFit;
	PlanePrimitiveShape *pl = 0;
	SpherePrimitiveShape *sp = 0;
	CylinderPrimitiveShape *cy = 0;
	ConePrimitiveShape *co = 0;
	MiscLib::Vector<DetectedShape> dshapes(shapes);
	shapes.clear();
	for (int si = 0;si < dshapes.size();si++) {
		PrimitiveShape* shapei = dshapes[si].first;
		std::cout << "Optimizing primitive " << si << std::endl;
		pcl::PointCloud<PCLPrimFit::PointT>::Ptr cloud(new pcl::PointCloud<PCLPrimFit::PointT>);
		pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
		pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
		pcl::PointIndices::Ptr inliers(new pcl::PointIndices);

		std::ofstream out(std::to_string(si) + ".xyz");
		for (int j = 0;j < primpts[si].size();j++) {

			Point pj = pcl[pcl.pix2id[primpts[si][j]]];
			cloud->push_back(PCLPrimFit::PointT(pj.pos[0], pj.pos[1], pj.pos[2]));
			cloud_normals->push_back(pcl::Normal(pj.normal[0], pj.normal[1], pj.normal[2]));

			out << pj.pos[0] << " " << pj.pos[1] << " " << pj.pos[2] << std::endl;
		}
		out.close();
		
		
		if (pclFit.Fit(cloud, cloud_normals, sacmodmap[dshapes[si].first->Identifier()], 
			coefficients, inliers) > 0) { // refinement succeeded
			switch (dshapes[si].first->Identifier()) {
			case(0):
				pl = dynamic_cast<PlanePrimitiveShape*>(shapei);
				shapes.push_back(DetectedShape(new PlanePrimitiveShape(Plane(pl->Internal().getPosition(), Vec3f(coefficients->values[0], coefficients->values[1], coefficients->values[2]))), dshapes[si].second));
				break;
			case(1):
				sp = dynamic_cast<SpherePrimitiveShape*>(shapei);
				shapes.push_back(DetectedShape(new SpherePrimitiveShape(Sphere(Vec3f(coefficients->values[0], coefficients->values[1], coefficients->values[2]), coefficients->values[3])), dshapes[si].second));
				break;
			case(2):
				cy = dynamic_cast<CylinderPrimitiveShape*>(shapei);
				shapes.push_back(DetectedShape(new CylinderPrimitiveShape(Cylinder(Vec3f(coefficients->values[3], coefficients->values[4], coefficients->values[5]),
					Vec3f(coefficients->values[0], coefficients->values[1], coefficients->values[2]), coefficients->values[6])), dshapes[si].second));
				break;
			case(3):
				co = dynamic_cast<ConePrimitiveShape*>(shapei);
				shapes.push_back(DetectedShape(new ConePrimitiveShape(Cone(Vec3f(coefficients->values[0], coefficients->values[1], coefficients->values[2]),
					Vec3f(coefficients->values[3], coefficients->values[4], coefficients->values[5]), coefficients->values[6])), dshapes[si].second));
				break;
			}
		}
		else 
			shapes.push_back(dshapes[si]);
	}
#endif
}
void ProbFit::savePrimitives(std::string prefix) {

	witePrimitives(prefix + "-pre.prim", shapes);

	std::ifstream in(prefix + "-prim.npz");
	if (in) {
		in.close();
		std::string fname(prefix + "-prim.npz");
		remove(fname.c_str());
	}

	CloudReader::saveKinectImg(prefix + "-prim.npz", cls, std::string("cls"));	
	CloudReader::saveKinectImg(prefix + "-prim.npz", ins, std::string("ins"));
}

int ProbFit::fitPrimitive(Options pbfopt, bool globcorr) {

	std::vector <PointCloud> pcls;
	
	PointCloud pcli;
	int npts = 0;
	int npcls = 0;
	switch (pbfopt.sel_method) {
	case(THRESH):
		npts = threshSelect(pbfopt.probID, pbfopt.threshs[pbfopt.probID], pcli);
		pcls.push_back(pcli);
		break;
	case(TOP2PROB):
		npts = top2ProbSelect(pbfopt.probID, pcli);
		pcls.push_back(pcli);
		break;
	case(MAXPROBWITHTHRESH):
		npts =  maxProbWithThreshSelect(pbfopt.probID, pbfopt.threshs[pbfopt.probID], pcli);
		pcls.push_back(pcli);
		break;
	case(INSTANCESEG):
		npcls = instanceSelect(pbfopt.probID, pbfopt.threshs[pbfopt.probID], pcls);
		break;
	default:
		npts = maxProbSelect(pbfopt.probID, pcli);
		pcls.push_back(pcli);
		break;
	}
	int count = 0;
	for (int i = 0;i < pcls.size();i++) {

		if (pcls[i].size() < pbfopt.m_minSupport) 
			continue;
		
		//std::ofstream out("pts.xyz");
		//for (int j = 0;j < pcls[i].size();j++)
		//	out << pcls[i][j].pos[0] << " " << pcls[i][j].pos[1] << " " << pcls[i][j].pos[2] << std::endl;
		//out.close();

		RansacShapeDetector detector(pbfopt);
		detector.Add(genPrimConstructor(pbfopt.primID));
		MiscLib::Vector< DetectedShape > shapei;
		std::vector<std::vector<int> > g_index;
		try {
			// if use geometry correction, not finished yet
			if (globcorr)
				detector.Detect(pcls[i], 0, pcl.size(), &shapei, &pcl, &g_index, pbfopt.probID);
			else// if do not use geometry correction
				detector.Detect(pcls[i], 0, pcls[i].size(), &shapei);
		}
		catch (std::exception& e) {
			std::cout << e.what() << std::endl;
		}

		count += shapei.size();

		for (MiscLib::Vector< DetectedShape >::iterator it = shapei.begin();it != shapei.end();it++) {
			shapes.push_back(*it);
			//break;// reserve only the first one
		}

		if (globcorr)
			calLabel(g_index, pbfopt.primID);
		else
			calLabel(pcls[i], shapei);
	}

	return count;
}

int ProbFit::threshSelect(int pi,double thresh, PointCloud& pcli) {

	uint8_t* selected = new uint8_t[480 * 640];
	memset(selected, 0, sizeof(uint8_t) * 480 * 640);

#ifndef QUIET
	std::ofstream out(std::to_string(pi) + ".xyz");
#endif
	for (int i = 0; i < pcl.size();i++) {
		//std::cout << pcl[i].probs[pi] << " " << pfopt.threshs[pi] << " " << (cls[pcl[i].pixID]<0) << std::endl;
		if (pcl[i].probs[pi] > thresh && cls[pcl[i].pixID]<0) {
			pcli.push_back(pcl[i]);
			selected[pcl[i].pixID] = 1;
#ifndef QUIET
			out << pcl[i].pos[0] << " " << pcl[i].pos[1] << " " << pcl[i].pos[2] << " ";
			out << pcl[i].normal[0] << " " << pcl[i].normal[1] << " " << pcl[i].normal[2] << std::endl;
#endif
		}
	}
#ifndef QUIET
	out.close();
	CloudReader::saveKinectImg(std::to_string(pi) + ".npy", selected);
#endif
	delete[] selected;
	return pcli.size();
}

int ProbFit::maxProbSelect(int pi, PointCloud& pcli) {
	uint8_t* selected = new uint8_t[480 * 640];
	memset(selected, 0, sizeof(uint8_t) * 480 * 640);
#ifndef QUIET
	std::ofstream out(std::to_string(pi) + ".xyz");
#endif // !NDEBUG

	for (int i = 0; i < pcl.size();i++) {
		int j;
		double pedge = 1;
		for (j = 0;j < pcl.n_probs;j++) {
			if (j != pi && pcl[i].probs[pi] < pcl[i].probs[j])
				break;
			pedge -= pcl[i].probs[j];
		}
		if (j==4 && pcl[i].probs[pi] > pedge) {
			pcli.push_back(pcl[i]);
			selected[pcl[i].pixID] = 1;
#ifndef QUIET
			out << pcl[i].pos[0] << " " << pcl[i].pos[1] << " " << pcl[i].pos[2] << " ";
			out << pcl[i].normal[0] << " " << pcl[i].normal[1] << " " << pcl[i].normal[2] << std::endl;
#endif 
		}
	}
#ifndef QUIET
	out.close();
	CloudReader::saveKinectImg(std::to_string(pi) + ".npy", selected);
#endif
	delete[] selected;
	return pcli.size();
}

int ProbFit::maxProbWithThreshSelect(int pi, double thresh, PointCloud& pcli) {
	uint8_t* selected = new uint8_t[480 * 640];
	memset(selected, 0, sizeof(uint8_t) * 480 * 640);
#ifndef QUIET
	std::ofstream out(std::to_string(pi) + ".xyz");
#endif // !NDEBUG

	for (int i = 0; i < pcl.size();i++) {
		if (pcl[i].probs[pi] < thresh)
			continue;
		int j;
		double pedge = 1;
		for (j = 0;j < pcl.n_probs;j++) {
			if (j != pi && pcl[i].probs[pi] < pcl[i].probs[j])
				break;
			pedge -= pcl[i].probs[j];
		}
		if (j == pcl.n_probs && pcl[i].probs[pi] > pedge) {
			pcli.push_back(pcl[i]);
			selected[pcl[i].pixID] = 1;
#ifndef QUIET
			out << pcl[i].pos[0] << " " << pcl[i].pos[1] << " " << pcl[i].pos[2] << " ";
			out << pcl[i].normal[0] << " " << pcl[i].normal[1] << " " << pcl[i].normal[2] << std::endl;
#endif 
		}
	}
#ifndef QUIET
	out.close();
	CloudReader::saveKinectImg(std::to_string(pi) + ".npy", selected);
#endif
	delete[] selected;
	return pcli.size();
}

int ProbFit::top2ProbSelect(int pi, PointCloud& pcli) {
	uint8_t* selected = new uint8_t[480 * 640];
	memset(selected, 0, sizeof(uint8_t) * 480 * 640);
#ifndef QUIET
	std::ofstream out(std::to_string(pi) + ".xyz");
#endif
	for (int i = 0; i < pcl.size();i++) {
		//std::cout << pcl[i].probs[pi] << " " << pfopt.threshs[pi] << " " << (cls[pcl[i].pixID]<0) << std::endl;
		int count = 0;
		double pedge = 1;
		for (int j = 0;j < pcl.n_probs;j++) {
			if (j != pi && pcl[i].probs[pi] > pcl[i].probs[j]) {
				count++;
				if(count>2) break;
			}
			pedge -= pcl[i].probs[j];
		}
		if (count>2 && pcl[i].probs[pi] > pedge) {
			pcli.push_back(pcl[i]);
			selected[pcl[i].pixID] = 1;
#ifndef QUIET
			out << pcl[i].pos[0] << " " << pcl[i].pos[1] << " " << pcl[i].pos[2] << " ";
			out << pcl[i].normal[0] << " " << pcl[i].normal[1] << " " << pcl[i].normal[2] << std::endl;
#endif
		}
	}
#ifndef QUIET
	out.close();
	CloudReader::saveKinectImg(std::to_string(pi) + ".npy", selected);
#endif
	delete[] selected;
	return pcli.size();
}

int ProbFit::instanceSelect(int pi, double thresh, std::vector<PointCloud>& pcls, double ethresh) {
	uint8_t* selected = new uint8_t[480 * 640];
	memset(selected, 0, sizeof(uint8_t) * 480 * 640);
#ifndef QUIET
	std::ofstream out(std::to_string(pi) + ".xyz");
#endif
	for (int i = 0; i < pcl.size();i++) {
		int j;
		double pedge = 1;

		for (j = 0;j < pcl.n_probs;j++) {
			if (j != pi && pcl[i].probs[pi] < pcl[i].probs[j])
				break;
			pedge -= pcl[i].probs[j];
		}
		if (j == pcl.n_probs && pcl[i].probs[pi] > pedge) {
			selected[pcl[i].pixID] = 1;
#ifndef QUIET
			out << pcl[i].pos[0] << " " << pcl[i].pos[1] << " " << pcl[i].pos[2] << " ";
			out << pcl[i].normal[0] << " " << pcl[i].normal[1] << " " << pcl[i].normal[2] << std::endl;
#endif
		}
	}
#ifndef QUIET
	out.close();
	CloudReader::saveKinectImg(std::to_string(pi) + ".npy", selected);
#endif
	
#ifdef BIASSFIT_OBJECTSEG
	std::vector<std::vector<int> > comps;
	int ncomp = connnected(selected, comps);

	for (int i = 0;i < comps.size();i++) {
		PointCloud pcli;
		for (int j = 0;j < comps[i].size();j++) {
			pcli.push_back(pcl[pcl.pix2id[comps[i][j]]]);
		}
		pcls.push_back(pcli);
	}

#endif

	delete[] selected;
	return pcls.size();
}

bool ProbFit::calLabel(PointCloud& cloud, MiscLib::Vector< DetectedShape >& shapes) {
	if (shapes.size() <= 0) return false;

	int count = cloud.size();

	for (MiscLib::Vector<DetectedShape>::const_iterator it = shapes.begin(); it != shapes.end(); ++it)
	{
		const PrimitiveShape* shape = it->first;
		unsigned shapePointsCount = static_cast<unsigned>(it->second);

		std::string desc;
		shape->Description(&desc);
		std::transform(desc.begin(), desc.end(), desc.begin(), ::tolower);

		std::vector<int> primi(it->second);
		for (unsigned j = 0; j < shapePointsCount; ++j) {
			primi[j] = cloud[count - 1 - j].pixID;
			cls[cloud[count - 1 - j].pixID] = labelmap[desc];
			ins[cloud[count - 1 - j].pixID] = insID;
		}
		primpts.push_back(primi);

		count -= shapePointsCount;
		insID++;
	}
	//save it to file

	return true;
}

bool ProbFit::calLabel(std::vector<std::vector<int> >& gindex, int primid) {

	for (int i = 0;i < gindex.size();i++) {
		for (int j = 0;j < gindex[i].size();j++) {
			cls[pcl[gindex[i][j]].pixID] = primid;
			ins[pcl[gindex[i][j]].pixID] = insID;
		}
		insID++;
	}

	return true;
}