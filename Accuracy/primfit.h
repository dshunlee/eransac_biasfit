#pragma once

#include "fitheader.h"

class PrimFit {
public:
	enum SELECTIONMETHOD { UNDEFINED = 0, MAXPROB = 1, THRESH = 2, MAXPROBWITHTHRESH = 3, TOP2PROB = 4, INSTANCESEG = 5 };

	class Options :public RansacShapeDetector::Options {
	public:
		Options() :primID(-1), probID(-1) {}
		Options(int primid, int probid) :primID(primid), probID(probid) {
			RansacShapeDetector::Options();
			setThresh();
			sel_method = UNDEFINED;
		}
		Options(int primid, int probid, unsigned minsup, float eps, float nthresh, float nthreshglob, float biteps, float prob) :
			RansacShapeDetector::Options(minsup, eps, nthresh, nthreshglob, biteps, prob), primID(primid), probID(probid) {
			setThresh();
			sel_method = UNDEFINED;
		}
		void setThresh(double* thr = 0) {
			if (thr == 0)
				memset(threshs, 0, 6 * sizeof(float));
			else
				memcpy(threshs, thr, sizeof(float) * 6);
		};
	public:
		float threshs[6];
		SELECTIONMETHOD sel_method;
		int primID;
		int probID;
	};

public:
	PrimFit() :insID(0) { allocateClsIns(); }
	~PrimFit() {
		if (cls != 0)
			delete[] cls;
		if (ins != 0)
			delete[] ins;
	}

	virtual bool loadData(std::string datfn);
	virtual bool loadData(std::string datfn, std::string labfn);
	bool allocateClsIns(int n = 1); // allocate memory to store labels 
	void savePrimitives(std::string prefix, bool seperate = false); // seperate: store the labels seperately or not
	bool calLabel(bool sepreate=false); //seperate: store the labels seperately or not

	int8_t* getClasPtr() { return cls; }
	int8_t* getInsPtr() { return ins; }

public:
	std::string datfname; // point cloud file name
	std::string labfname; // prob file
	PointCloud pcl; // the point cloud
	MiscLib::Vector< DetectedShape > shapes; // recoard of shapes detected
	std::vector<std::vector<int> > primpts; // record pixel id of the primitives
	int primid; // primitive type id

	SELECTIONMETHOD selMeth;

protected:
	int8_t* cls;
	int8_t* ins;
	int insID; // instance id

	
};

PrimitiveShapeConstructor* genPrimConstructor(int primi);
PrimFit::Options parsePbfOpt(std::string str);
void readconfig(char* fname, std::vector<PrimFit::Options>& config);

