#pragma once

#include "PointCloud.h"

#include "PlanePrimitiveShapeConstructor.h"
#include "ConePrimitiveShapeConstructor.h"
#include "SpherePrimitiveShapeConstructor.h"
#include "CylinderPrimitiveShapeConstructor.h"

#include "PlanePrimitiveShape.h"
#include "SpherePrimitiveShape.h"
#include "ConePrimitiveShape.h"
#include "CylinderPrimitiveShape.h"

#include "RansacShapeDetector.h"

#include<map>

#define _USE_MATH_DEFINES
#include <math.h>

#ifndef CC_DEG_TO_RAD
#define CC_DEG_TO_RAD (M_PI/180.0)
#endif

typedef std::pair< MiscLib::RefCountPtr< PrimitiveShape >, size_t > DetectedShape;

static std::map<std::string, int8_t> labelmap = { { "plane",0 },{ "sphere",1 },{ "cylinder",2 },{ "cone",3 } };
