#ifdef BIASSFIT_OBJECTSEG

/* ceres primitive fitting functors
	currently, only sphere is implemented
*/

#pragma once

#include "PointCloud.h" 
#include "GfxTL\IndexedIterator.h"
#include "ceres/ceres.h"

class SphereCostFunctor;

template< class IteratorT >
bool ceresFit(int primid, double* params, IteratorT begin, IteratorT end, double sigma = 0.001) { // sigma is the std relative to depth(z)
	ceres::Problem problem;
	
	for (auto it = begin; it != end;it++) {
		SphereCostFunctor cost_function(it->pos);
		ceres::LossFunctionWrapper* loss_function = 
			new ceres::LossFunctionWrapper(new ceres::HuberLoss(3 * sigma*it->pos[2] * it->pos[2]), ceres::TAKE_OWNERSHIP); // 3 sigma error

		problem.AddResidualBlock(cost_function.genFunctor(), NULL, params);
	}

	ceres::Solver::Options options;
	options.trust_region_strategy_type = ceres::LEVENBERG_MARQUARDT;
	options.minimizer_progress_to_stdout = true;
	options.max_num_iterations = 200;
	options.function_tolerance = 1e-10;
	ceres::Solver::Summary summary;
	ceres::Solve(options, &problem, &summary);
	std::cout << summary.BriefReport() << "\n";

	return true;
}


class PrimCostFunctor{
public:
	PrimCostFunctor(const Vec3f& pt) : pt_(pt) {}
	virtual ceres::CostFunction* genFunctor() = 0 { return 0; }
protected:
	const Vec3f& pt_;
};

class SphereCostFunctor: public PrimCostFunctor {
public:
	SphereCostFunctor(const Vec3f& pt) :PrimCostFunctor(pt){}
	template <typename T>
	bool operator()(const T* const x, T* e) const {
		e[0] = sqrt((double(pt_[0]) - x[0])*(double(pt_[0]) - x[0]) 
			+ (double(pt_[1]) - x[1])*(double(pt_[1]) - x[1]) + (double(pt_[2]) - x[2])*(double(pt_[2]) - x[2])) - x[3];
		return true;
	}
	ceres::CostFunction* genFunctor() { 
		return new ceres::AutoDiffCostFunction<SphereCostFunctor, 1, 4>(new SphereCostFunctor(pt_)); }
};

#endif // BIASSFIT_OBJECTSEG