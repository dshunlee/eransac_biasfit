#ifdef BIASSFIT_OBJECTSEG

#include "primransac.h"
#include "ceresprimfit.h"
#include <fstream>

int PrimRansac::fit(std::vector<Options > pbfopt) {
	std::map<int, Options> optmap;
	for (int i = 0;i < pbfopt.size();i++) {
		optmap[pbfopt[i].primID] = pbfopt[i];
	}
	for (int i = 0;i < pbfopt.size();i++) {
		Options opti = pbfopt[i];
		double prob = 1 - opti.m_probability;
		if(shapecon) shapecon->Release();
		shapecon =  genPrimConstructor(opti.primID);
		int n = shapecon->RequiredSamples();
		nK = log(1 - prob) / log(1 - pow(omg, n));

		findComps(&opti,0.1);
		int nprimi = fiti(opti);
	}
	return shapes.size();
}

bool ceresFit(const PointCloud &pc, int primid, double* params,
	MiscLib::Vector< size_t >::const_iterator begin,
	MiscLib::Vector< size_t >::const_iterator end, double sigma){
	bool retVal = ceresFit(primid, params, GfxTL::IndexIterate(begin, pc.begin()),
		GfxTL::IndexIterate(end, pc.begin()));
	return retVal;
}

static PrimitiveShape* ceresPrimFit(PrimitiveShape* shape, PointCloud& pcl, MiscLib::Vector<size_t>& idx) {
	double params[7];
	if (shape->Identifier() == 0) { // is plane


	}
	else if (shape->Identifier() == 1) { // is sphere, currently only sphere is implemented with ceres
		SpherePrimitiveShape *prim = dynamic_cast<SpherePrimitiveShape*>(shape);
		params[0] = prim->Internal().Center()[0];
		params[1] = prim->Internal().Center()[1];
		params[2] = prim->Internal().Center()[2];
		params[3] = prim->Internal().Radius();
		ceresFit(pcl, 1, params, idx.begin(), idx.end());

		return new SpherePrimitiveShape(Sphere(Vec3f(params[0], params[1], params[2]), params[3]));
	}
	else if(shape->Identifier() == 2){ // is cylinder

	}
	else if (shape->Identifier() == 3) {// is cone

	}
}

int PrimRansac::fiti(const Options& opti) {
	if (comps.empty()) return 0;
	pcl.genPix2IDMap();
	// sort component based on number of points
	std::vector<std::pair<int, int> > pclsize;
	for (int i = 0;i < comps.size();i++)
		pclsize.push_back(std::pair<int, int>(comps[i].size(), i));
	std::sort(pclsize.begin(), pclsize.end());

	int s = comps.size() - 1; // start from largest comp
	std::vector<bool> flag(comps.size(), true);

	int nprims = 0;
	do {
		if (!flag[s]) {
			s--;
			continue;
		}
		if (pclsize[s].first < opti.m_minSupport)
			return nprims;

		flag[s] = false;
		// generate candidate from comp ci
		int ci = pclsize[s].second;
		PrimitiveShape *bestShape = NULL;

		MiscLib::Vector<size_t> idx(pclsize[s].first);
		for (int j = 0;j < pclsize[s].first;j++)
			idx[j] = pcl.pix2id[comps[ci][j]];
		std::pair< size_t, float > score(0,0);
		int nKi = nK;
		while (nKi >= 0) {
			// genrate samples
			std::set<int> spid;
			MiscLib::Vector<Vec3f> samples(shapecon->RequiredSamples()*2);
			int nsmp = 0; 
			while (nsmp < shapecon->RequiredSamples()) {
				int id = rand() % pclsize[s].first;
				if (spid.count(id)) // already exist
					continue;
				spid.insert(id);
				samples[nsmp] = pcl[pcl.pix2id[comps[ci][id]]].pos;
				samples[nsmp + shapecon->RequiredSamples()] = pcl[pcl.pix2id[comps[ci][id]]].normal;
				nsmp++;
			}
			
			PrimitiveShape* inishape = shapecon->Construct(samples);
			
			if (inishape == NULL) continue;

			PrimitiveShape *bestShapei = NULL;

			//std::ofstream out("dd.xyz");
			//for (int i = 0;i < idx.size();i++)
			//	out << pcl[idx[i]].pos[0] << " " << pcl[idx[i]].pos[1] << " " << pcl[idx[i]].pos[2] << " "<< pcl[idx[i]].normal[0] << " " << pcl[idx[i]].normal[1] << " " << pcl[idx[i]].normal[2] << std::endl;
			//out.close();
			std::pair< size_t, float > scorei;
			bestShapei = inishape->LSFit(pcl, opti.m_epsilon, opti.m_normalThresh, idx.begin(), idx.end(), &scorei);
			if (!bestShapei) 
				bestShapei = inishape;
			else
				inishape->Release();

			scorei.first = 0;
			std::pair<float, float> dn;
			for (int j = 0;j < pclsize[s].first;j++) {
				int pid = pcl.pix2id[comps[ci][j]];
				bestShapei->DistanceAndNormalDeviation(pcl[pid].pos, pcl[pid].normal, &dn);
				if (dn.first < 3 * opti.m_epsilon*pcl[pid].pos[2] * pcl[pid].pos[2] && dn.second >= opti.m_normalThresh) // distance thresh 3 sigma
					scorei.first++;
			}
			scorei.second = 0;
			for ( int i = 0; i < idx.size(); ++i)
				scorei.second += weigh(bestShapei->Distance(pcl[idx[i]].pos), 3 * opti.m_epsilon*opti.m_epsilon*pcl[idx[i]].pos[2] * pcl[idx[i]].pos[2]);
			
			if (scorei.first > score.first  && scorei.second > score.second) {
				if(bestShape) bestShape->Release();
				bestShape = bestShapei;
				score = scorei;
			}
			//bestShapei->Release();

			nKi--;
		}
		if (bestShape) {

			//PrimitiveShape *bestShapej = ceresPrimFit(bestShape, pcl, idx);

			int counti = pclsize[s].first;
			std::vector<int> primi;
			primi.insert(primi.end(), comps[ci].begin(), comps[ci].end());

			for (int j = s - 1; j >= 0 && flag[j];j--) { // combine smaller components if they fit with the primitive
				if (judgeCompSamp(pcl, comps[pclsize[j].second], bestShape,opti)) {
					counti += pclsize[j].first;
					primi.insert(primi.end(), comps[pclsize[j].second].begin(), comps[pclsize[j].second].end());
					flag[j] = false;
				}
			}
			primpts.push_back(primi);
			

			if (primi.size() > idx.size()) { // if new pts added 
				idx.resize(primi.size());
				for (int j = 0;j < primi.size();j++)
					idx[j] = pcl.pix2id[primi[j]];
				// do fitting again with all pts
				bestShape->LSFit(pcl, opti.m_epsilon, opti.m_normalThresh, idx.begin(), idx.end(), &score);
			}
			nprims++;
			shapes.push_back(DetectedShape(bestShape, counti));
			bestShape->Release();
		}
		s--;
	} while (s >= 0);

	return nprims;
}

int PrimRansac::findComps(const Options* opt, double pedge) {
	comps.clear();
	uint8_t* selected = new uint8_t[480 * 640];
	memset(selected, 0, sizeof(uint8_t) * 480 * 640);

	if (opt) { // maxprob selection plus the edge is threshed out
		int pi = opt->probID;
		for (int i = 0; i < pcl.size();i++) {
			int j;
			double pedgei = 1;
			for (j = 0;j < pcl.n_probs;j++) {
			if (j != pi && pcl[i].probs[pi] < pcl[i].probs[j])
				break;
			pedgei -= pcl[i].probs[j];
			}
			if (j == pcl.n_probs && pcl[i].probs[pi] > pedgei && pcl[i].probs[0]<pedge) {
				selected[pcl[i].pixID] = 1;
			}
		}
	}
	else { // use edge to segment, primitive type unknown
		for (int i = 0; i < pcl.size();i++) {

			if (pcl[i].probs[0] < pedge) {
				selected[pcl[i].pixID] = 1;
			}
		}
	}

	int ncomp = connnected(selected, comps);

	delete[] selected;
	return ncomp - 1;
}
bool PrimRansac::judgeCompSamp(PointCloud& pcl, std::vector<int> ci, PrimitiveShape* prim, const Options& opti, int n) {
	std::pair<float, float> dn;
	int ninliersi = 0;
	for (int i = 0; i < n;i++) {
		int id = rand() % ci.size();
		id = pcl.pix2id[ci[id]];
		prim->DistanceAndNormalDeviation(pcl[id].pos, pcl[id].normal, &dn);
		if (dn.first < 3 * opti.m_epsilon*pcl[id].pos[2] * pcl[id].pos[2] && dn.second >= opti.m_normalThresh) // distance thresh 3 sigma
			ninliersi++;
	}
	if (ninliersi > 0.5*n)
		return true;
	return false;
}

#endif BIASSFIT_OBJECTSEG