
#ifdef SEGMENT_REGIONGROW

#pragma once
#include <iostream>
#include <vector>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include "probfit.h"
//#include <pcl/io/pcd_io.h>

#include "fitheader.h"

#include <vector>

class RegionGrow {
public:
	RegionGrow():cloud(0),normals(0){}
	void fit(std::string fname, std::string prefix, PrimFit::Options opt, int minsupport=1000, int K=30, double angthresh=5, double curvthresh=2,bool estnorm=true);
	
private:
	void loadPcl(std::string fname,bool estnorm=true);
public:
	pcl::PointCloud<pcl::PointXYZ>::Ptr cloud;
	pcl::PointCloud <pcl::Normal>::Ptr normals;
private:
	std::vector<int> id2pix;
};

#endif
