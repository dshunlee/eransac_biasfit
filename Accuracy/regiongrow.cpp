
#ifdef SEGMENT_REGIONGROW

#pragma once
#include "regiongrow.h"
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/passthrough.h>
#include <pcl/segmentation/region_growing.h>
#include <pcl/io/pcd_io.h>
#include "cnpy.h"
#include "primitiveio.h"

void RegionGrow::loadPcl(std::string fname,bool estnorm) {
	cnpy::NpyArray xyz = cnpy::npz_load(fname,"data");
	cnpy::NpyArray norm = cnpy::npz_load(fname, "normal");

	int Npts = 480*640;
	float *xyzptr = reinterpret_cast<float*>(xyz.data);
	float *normptr = reinterpret_cast<float*>(norm.data);
	
	cloud = pcl::PointCloud<pcl::PointXYZ>::Ptr(new pcl::PointCloud<pcl::PointXYZ>);
	normals = pcl::PointCloud <pcl::Normal>::Ptr(new pcl::PointCloud <pcl::Normal>);

	cloud->reserve(Npts);
	if(!estnorm)
		normals->reserve(Npts);
	int Npts2 = 2*Npts;
	for (int i = 0;i < Npts;i++, xyzptr++,normptr++) {
		if (*xyzptr==0 || *(xyzptr + Npts)==0 || *(xyzptr + Npts2)==0)
			continue;

		cloud->push_back(pcl::PointXYZ(*xyzptr, *(xyzptr + Npts), *(xyzptr + Npts2)));
		if(!estnorm)
			normals->push_back(pcl::Normal(*normptr, *(normptr + Npts), *(normptr + Npts2)));

		id2pix.push_back(i);
	}
	cloud->resize(id2pix.size());
	if (!estnorm)
		normals->resize(id2pix.size());
	else {
		pcl::search::Search<pcl::PointXYZ>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZ> >(new pcl::search::KdTree<pcl::PointXYZ>);
		pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> normal_estimator;
		normal_estimator.setSearchMethod(tree);
		normal_estimator.setInputCloud(cloud);
		normal_estimator.setKSearch(50);
		normal_estimator.compute(*normals);
	}
}

void RegionGrow::fit(std::string fname, std::string prefix, PrimFit::Options opt, int minsupport, int K, double angthresh, double curvthresh, bool estnorm) {
	//pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
	//pcl::PointCloud <pcl::Normal>::Ptr normals(new pcl::PointCloud <pcl::Normal>);

	loadPcl(fname, estnorm);

	pcl::search::Search<pcl::PointXYZ>::Ptr tree = boost::shared_ptr<pcl::search::Search<pcl::PointXYZ> >(new pcl::search::KdTree<pcl::PointXYZ>);
	pcl::RegionGrowing<pcl::PointXYZ, pcl::Normal> reg;
	reg.setMinClusterSize(minsupport);
	reg.setMaxClusterSize(3000000);
	reg.setSearchMethod(tree);
	reg.setNumberOfNeighbours(K);
	reg.setInputCloud(cloud);
	//reg.setIndices (indices);
	reg.setInputNormals(normals);
	reg.setSmoothnessThreshold(angthresh / 180.0 * M_PI);
	reg.setCurvatureThreshold(curvthresh);
	//reg.setResidualThreshold

	std::vector <pcl::PointIndices> clusters;
	reg.extract(clusters);

	pcl::PointCloud <pcl::PointXYZRGB>::Ptr colored_cloud = reg.getColoredCloud();
	pcl::io::savePCDFileBinary(prefix + ".pcd", *colored_cloud);
	
	std::cout << clusters.size() << " clusters found!" << std::endl;

	RansacShapeDetector detector(opt);
	detector.Add(new PlanePrimitiveShapeConstructor());
	detector.Add(new SpherePrimitiveShapeConstructor());
	detector.Add(new CylinderPrimitiveShapeConstructor());
	detector.Add(new ConePrimitiveShapeConstructor());

	int8_t *seg = new int8_t[480 * 640];
	memset(seg, -1, sizeof(int8_t) * 480 * 640);
	int primi = 0;
	MiscLib::Vector< DetectedShape > shapes;
	for (int i = 0;i < clusters.size() && primi<128;i++) {
		PointCloud pc;
		for (int j = 0;j < clusters[i].indices.size();j++) {
			Point p(id2pix[clusters[i].indices[j]], Vec3f(cloud->at(clusters[i].indices[j]).x, cloud->at(clusters[i].indices[j]).y, cloud->at(clusters[i].indices[j]).z),
				Vec3f(normals->at(clusters[i].indices[j]).normal_x, normals->at(clusters[i].indices[j]).normal_y, normals->at(clusters[i].indices[j]).normal_z));
			pc.push_back(p);
		}
		MiscLib::Vector< DetectedShape > shapei;
		detector.Detect(pc, 0, pc.size(), &shapei);
		std::cout << shapei.size()<<" primitives found in cluster "<<i<<"!" << std::endl;
		int count = pc.size()-1;
		for (int j = 0;j < shapei.size() && primi<128;j++) {
			shapes.push_back(shapei[j]);
			for (int k = 0;k < shapei[j].second;k++,count--)
				seg[pc[count].pixID] = primi;

			primi++;

			if (primi == 128)
				std::cout << "primitive number larger than 128, only the first 128 primitives are recorded!" << std::endl;
		}
	}
	std::cout << shapes.size() << " primitives fitted!" << std::endl;
	unsigned int shape[2] = { 480,640 };
	cnpy::npz_save(prefix+".npz", "ins", seg, shape, 2);
	witePrimitives(prefix + ".prim", shapes);

	delete[] seg;
}

#endif