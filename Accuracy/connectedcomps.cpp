#ifdef BIASSFIT_OBJECTSEG

#include "connectedcomps.h"

#include "opencv2/imgproc.hpp"
//#include <opencv2/core/utility.hpp>
#include <opencv2\highgui.hpp>

//#define OUTPUTCOMPONENT

int connnected(uint8_t* img, std::vector<std::vector<int> >& comps) {
	
	cv::Mat bimg(480, 640, CV_8U, img);
	cv::Mat element = cv::getStructuringElement(cv::MORPH_CROSS,
		cv::Size(3, 3),
		cv::Point(1, 1));

	for(int i=0;i<1;i++)
		cv::erode(bimg, bimg, element);
	
	cv::Mat labelImage(bimg.size(), CV_16U);
	int nLabels = cv::connectedComponents(bimg, labelImage, 8, CV_32S);

	if (nLabels > 1)
	{
		for (int i = 0;i < nLabels - 1;i++) 
			comps.push_back(std::vector<int>());
		int id = 0;
		for (int r = 0; r < labelImage.rows; ++r) 
			for (int c = 0; c < labelImage.cols; ++c, ++id) 
				if(labelImage.at<int>(r, c)>0)
					comps[labelImage.at<int>(r, c) - 1].push_back(id);
	}

	for (int i = 0;i < comps.size();) {
		if (comps[i].size() < 500)
			comps.erase(comps.begin() + i);
		else
			i++;
		//n = comps.size();
	}
	
#ifdef OUTPUTCOMPONENT
	cv::imwrite("img.jpg", bimg * 255);
	
	std::vector<cv::Vec3b> colors(nLabels);
	//colors[0] = cv::Vec3b(0, 0, 0);//background
	for (int label = 0; label < nLabels; ++label) {
		colors[label] = cv::Vec3b((rand() & 255), (rand() & 255), (rand() & 255));
	}
	cv::Mat dst(bimg.size(), CV_8UC3);
	for (int r = 0; r < dst.rows; ++r) {
		for (int c = 0; c < dst.cols; ++c) {
			int label = labelImage.at<int>(r, c);
			cv::Vec3b &pixel = dst.at<cv::Vec3b>(r, c);
			pixel = colors[label];
		}
	}
	cv::imwrite("comps.jpg", dst);
#endif 

	return comps.size();
}

#endif // BIASSFIT_OBJECTSEG