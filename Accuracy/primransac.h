
#ifdef BIASSFIT_OBJECTSEG


#pragma once

#include <math.h>
#include <vector>
#include <set>

#include "fitheader.h"
#include "primfit.h"
#include "connectedcomps.h"

class PrimRansac: public PrimFit{
public:
	PrimRansac(double omega = 0.5): omg(omega), shapecon(0){}
	
	int fit(std::vector<Options > pbfopt);
private:
	int fiti(const Options& opti);
	int findComps(const Options* opt=0, double pedge=0.1);
	bool judgeCompSamp(PointCloud& pcl, std::vector<int> ci, PrimitiveShape* prim, const Options& opti, int n = 50);
public:
	PrimitiveShapeConstructor* shapecon;
	std::vector<std::vector<int> > comps;

private:
	// omega and nK for RANSAC
	double omg;
	double nK;
};

//bool ceresFit(const PointCloud &pc, int primid, double* params,
//	MiscLib::Vector< size_t >::const_iterator begin,
//	MiscLib::Vector< size_t >::const_iterator end, double sigma = 0.001);

#endif // BIASSFIT_OBJECTSEG