#pragma once

#include "PointCloud.h"
#include <string>
#include <fstream>
#include "cnpy.h"

namespace CloudReader {
	// read data from xyz file, coordinates only
	int read_xyz(std::string fname, PointCloud& pcl);
	/*read data from npz file
	the npz file should have the fields:
	"data": coordinates in 3 x h x w format 
	"normal_est": normal in 3 x h x w format
	"normal": normal in 3 x h x w format
	"data" and eithor "normal_est" or "normal" are required, if both appeared, "normal_est" will be used
	*/
	int read_npz(std::string fname, PointCloud& pcl);
	// read data and results obtained from biasfit 
	/*read data and results obtained from biasfit 
	the data npz file should have the fields:
	"data": coordinates in 3 x h x w format
	"normal_est": normal in 3 x h x w format
	"normal": normal in 3 x h x w format
	"data" and eithor "normal_est" or "normal" are required, if both appeared, "normal_est" will be used
	the label npz file should have the fields:
	"probs": probability map in n x h x w format, n >= 4
		the 1st prob is for boundary if boundary is stored
		following 4 probs are for plane, sphere, cylinder and cone
		last prob is for nurbs
	*/
	int read_data_label(std::string datfn, std::string labfn, PointCloud& pcl);
	/*read data results from maskrcnn
	the data npz file should have the fields:
	"data": coordinates in 3 x h x w format
	"normal_est": normal in 3 x h x w format
	"normal": normal in 3 x h x w format
	"data" and eithor "normal_est" or "normal" are required, if both appeared, "normal_est" will be used
	the label npz file should have the fields:
	"mask": mask in h x w x n format, n is the number of instance
	"score": object confidence score, list len n
	"cls": predicted class label, list len n, 1 for plane, 2 for sphere, 3 for cylinder, 4 for cone, same order as biasfit
	*/
	int read_data_label_maskrcnn(std::string datfn, std::string labfn, PointCloud& pcl);

	template<typename T>
	void save(std::string fname, T* data, int channel) {
		const unsigned int shape[] = { channel, 480,640 };
		cnpy::npy_save(fname, data, shape, 3, "w");
	}
	template<typename T>
	void saveKinectImg(std::string fname, T* data, int channel, std::string dataname) {
		const unsigned int shape[] = { channel,480,640 };
		
		std::ifstream in(fname);
		if (in) {
			in.close();
			cnpy::npz_save(fname, dataname, data, shape, 3, "w");
		}
		else {
			cnpy::npz_save(fname, dataname,  data, shape, 3, "a");
		}
	}
	template<typename T>
	void saveKinectImg(std::string fname, T* data) {
		const unsigned int shape[] = { 480,640 };
		cnpy::npy_save(fname, data, shape, 2, "w");
	}
	template<typename T>
	void saveKinectImg(std::string fname, T* data, std::string dataname, int n=1) {
		unsigned int shape[3] = { 480,640,1 };
		int dim = 2;
		if (n > 1) {
			shape[0] = n;
			shape[1] = 480;
			shape[2] = 640;
			dim = 3;
		}

		std::ifstream in(fname);
		if (in) {
			in.close();
			cnpy::npz_save(fname, dataname, data, shape, dim, "a");
		}
		else {
			cnpy::npz_save(fname, dataname, data, shape, dim, "w");
		}
	}
	template<typename T>
	int loadAttr(std::string npzf, T* cls, std::string attr) {
		cnpy::NpyArray clsarr = cnpy::npz_load(npzf, attr);
		int nbytes = clsarr.word_size / (480 * 640);
		memcpy(cls, clsarr.data, 480 * 640 * sizeof(T));
		return nbytes;
	}
};
