

#include "maskrcnnfit.h"

#include "primitiveio.h"
#include "cnpycloud.h"


bool MaskRcnnFit::loadData(std::string datfn, std::string labfn) {
	datfname = datfn;
	labfname = labfn;

	int npts = CloudReader::read_data_label_maskrcnn(datfn, labfn, pcl);

	pcl.genPix2IDMap();

	return npts>0;

}

int MaskRcnnFit::fit(std::vector<Options > pbfopt) {
	std::map<int, Options> optmap;
	for (int i = 0; i < pbfopt.size(); i++) {
		optmap[pbfopt[i].primID] = pbfopt[i];
	}
	for (int i = 0; i < pbfopt.size(); i++) {
		Options opti = pbfopt[i];
		omg = opti.threshs[opti.probID];
		if (shapecon) shapecon->Release();
		shapecon = genPrimConstructor(opti.primID);
		int n = shapecon->RequiredSamples();
		nK = log(opti.m_probability) / log(1 - pow(omg, n));
		bool suc;
		for (int j=0;j<pcl.n_masks;j++)
			if(pcl.n_maskrcnn_clas[j]==opti.probID) // probID is the maskrcnn category id 
				suc = fiti(j, opti);
	}
	return shapes.size();
}
MiscLib::Vector<size_t> MaskRcnnFit::find_object(int obji) {
	MiscLib::Vector<size_t> objid(pcl.n_maskrcnn_cnt[obji]);
	for (int i = 0; i < pcl.n_object_idx[obji].size(); i++) {
			objid[i] = pcl.n_object_idx[obji][i];
	}
	return objid;
}
bool MaskRcnnFit::fiti(int obji, const Options& opti) {

	// generate candidates from object i
	if (pcl.n_maskrcnn_score[obji] < opti.m_bitmapEpsilon) // use m_bitmapEpsilon to simplify option parsing
		return false; // check object confidence score

	int NPTS = pcl.n_maskrcnn_cnt[obji];
	if (NPTS < opti.m_minSupport)
		return false; // check object size

	// find pixels of object i 
	MiscLib::Vector<size_t> idx = find_object(obji);
	PrimitiveShape *bestShape = NULL;

	std::pair< size_t, float > score(0, 0);
	int nKi = nK;
	while (nKi >= 0) {
		// genrate samples
		std::set<int> spid;
		MiscLib::Vector<Vec3f> samples(shapecon->RequiredSamples() * 2);
		int nsmp = 0;
		while (nsmp < shapecon->RequiredSamples()) {
			int id = rand() % NPTS;
			if (spid.count(id)) // already exist
				continue;
			spid.insert(id);
			samples[nsmp] = pcl[idx[size_t(id)]].pos;
			samples[nsmp + shapecon->RequiredSamples()] = pcl[idx[size_t(id)]].normal;
			nsmp++;
		}
		// build an initial shape from samples
		PrimitiveShape* inishape = shapecon->Construct(samples);

		if (inishape == NULL) continue;

		PrimitiveShape *bestShapei = NULL;

		//std::ofstream out("dd.xyz");
		//for (int i = 0;i < idx.size();i++)
		//	out << pcl[idx[i]].pos[0] << " " << pcl[idx[i]].pos[1] << " " << pcl[idx[i]].pos[2] << " "<< pcl[idx[i]].normal[0] << " " << pcl[idx[i]].normal[1] << " " << pcl[idx[i]].normal[2] << std::endl;
		//out.close();

		std::pair< size_t, float > scorei;
		bestShapei = inishape->LSFit(pcl, opti.m_epsilon, opti.m_normalThresh, idx.begin(), idx.end(), &scorei);
		if (!bestShapei) // least square fitting failed
			bestShapei = inishape;
		else
			inishape->Release();

		scorei.first = 0;
		std::pair<float, float> dn;
		int pid;

		for (int j = 0; j < NPTS; j++) {
			pid = idx[j];
			bestShapei->DistanceAndNormalDeviation(pcl[pid].pos, pcl[pid].normal, &dn);
			if (dn.first < 3 * opti.m_epsilon*pcl[pid].pos[2] * pcl[pid].pos[2] && dn.second >= opti.m_normalThresh) // distance thresh 3 sigma
				scorei.first++;
		}

		for (int i = 0; i < idx.size(); ++i)
			scorei.second += weigh(bestShapei->Distance(pcl[idx[i]].pos), 3 * opti.m_epsilon*opti.m_epsilon*pcl[idx[i]].pos[2] * pcl[idx[i]].pos[2]);

		if (scorei.first > score.first  && scorei.second > score.second) {
			if (bestShape) bestShape->Release();
			bestShape = bestShapei;
			score = scorei;
		}
		//bestShapei->Release();

		nKi--;
	}

	if (bestShape) {
		//
		std::vector<int> primi(idx.size());
		for (int i = 0; i < NPTS; i++)
			primi[i] = pcl[size_t(idx[i])].pixID;
		primpts.push_back(primi);

		shapes.push_back(DetectedShape(bestShape, NPTS));
		bestShape->Release();
	}

	if (bestShape) return true;
	return false;
}