#include "primfit.h"
#include "primitiveio.h"
#include "cnpycloud.h"

bool PrimFit::loadData(std::string datfn) {
	datfname = datfn;

	return CloudReader::read_npz(datfn, pcl)>0;
}

bool PrimFit::allocateClsIns(int n) {
	int imgsize = 480 * 640;
	if (cls) {
		delete[] cls; 
		cls = 0;
	}
	if (ins) {
		delete[] ins;
		ins = 0;
	}
	try {
		cls = new int8_t[imgsize];
		memset(cls, -1, sizeof(int8_t) * imgsize);
		ins = new int8_t[imgsize*n];
		memset(ins, -1, sizeof(int8_t) * imgsize * n);
	}
	catch (std::exception& e) {
		std::cout << e.what() << std::endl;
		return false;
	}
	return true;
}
bool PrimFit::loadData(std::string datfn, std::string labfn) {
	datfname = datfn;
	labfname = labfn;

	int npts = CloudReader::read_data_label(datfn, labfn, pcl);

	pcl.genPix2IDMap();

	return npts>0;

}

bool PrimFit::calLabel(bool sepreate) {
	if (pcl.pix2id.empty())
		pcl.genPix2IDMap();
	int npix = 480 * 640;

	if (sepreate) // reallocate the memory
		allocateClsIns(shapes.size());

	int8_t* cls_ptr = cls;
	int8_t* ins_ptr = ins;
	
	for (int i = 0;i < primpts.size();i++, cls_ptr++) {
		primid =  shapes[i].first->Identifier();
		for (int j = 0;j < primpts[i].size();j++) {
			cls_ptr[primpts[i][j]] = primid;
			ins_ptr[primpts[i][j]] = insID;
		}

		if (sepreate) 
			ins_ptr += npix;
		
		insID++;
	}
	return true;
}

void PrimFit::savePrimitives(std::string prefix, bool seperate) {

	witePrimitives(prefix + ".prim", shapes);

	std::ifstream in(prefix + ".npz");
	if (in) {
		in.close();
		std::string fname(prefix + ".npz");
		remove(fname.c_str());
	}
	int n = 1;
	if (seperate && shapes.size()>0) n = shapes.size();
	CloudReader::saveKinectImg(prefix + ".npz", cls, std::string("cls"));
	CloudReader::saveKinectImg(prefix + ".npz", ins, std::string("ins"),n);
}

PrimitiveShapeConstructor* genPrimConstructor(int primi) {
	switch (primi) {
	case(0):
		return new PlanePrimitiveShapeConstructor();
	case(1):
		return new SpherePrimitiveShapeConstructor();
	case(2):
		return new CylinderPrimitiveShapeConstructor();
	case(3):
		return new ConePrimitiveShapeConstructor();
	default:
		return 0;
	}
}
PrimFit::Options parsePbfOpt(std::string str) {
	int prim, prob;
	int sel = 1;

	std::stringstream ss(str);
	ss >> prim >> prob;
	PrimFit::Options opt(prim, prob, 1000, 0.03, std::cos(30 * CC_DEG_TO_RAD), std::cos(30 * CC_DEG_TO_RAD), 0.05, 0.0001);
	ss >> opt.m_minSupport >> opt.m_epsilon >> opt.m_normalThresh >> opt.m_normalThresh_glob >> opt.m_bitmapEpsilon >> opt.m_probability;
	opt.m_normalThresh = std::cos(opt.m_normalThresh * CC_DEG_TO_RAD);
	opt.m_normalThresh_glob = std::cos(opt.m_normalThresh_glob * CC_DEG_TO_RAD);
	ss >> opt.threshs[prob];
	if (opt.threshs[prob] > 0) 
		sel = 3;
	ss >> sel;
	opt.sel_method = static_cast<PrimFit::SELECTIONMETHOD>(sel);
	return opt;
}

void readconfig(char* fname, std::vector<PrimFit::Options>& config) {
	std::ifstream in(fname);
	while (!in.eof()) {
		std::string line;
		std::getline(in, line);

		if (line.empty() || *(line.begin() + line.find_first_not_of(' ')) == '#')
			continue;

		config.push_back(parsePbfOpt(line));
	}
}
