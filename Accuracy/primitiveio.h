#pragma once

#include "fitheader.h"

bool witePrimitives(std::string fname, MiscLib::Vector<DetectedShape>& shapes);

template<typename T1, typename T2>
int readPrimitives(std::string fname, MiscLib::Vector<MiscLib::RefCountPtr< PrimitiveShape > >& prims, std::map<T1, T2>& ins2cls) {
	std::ifstream in(fname);
	
	if (!in) return 0;

	int count = 0;
	while (!in.eof()) {

		std::string line;
		std::getline(in, line);

		std::stringstream ss(line);
		std::string pname;
		ss >> pname;
		float x, y, z, nx, ny, nz, val;
		if (pname == "Plane") {
			ss >> x >> y >> z >> nx >> ny >> nz;
			PlanePrimitiveShape* prim = new PlanePrimitiveShape(Plane(Vec3f(x, y, z), Vec3f(nx, ny, nz)));
			prims.push_back(prim);
			prim->Release();
			count++;
			//out << pl.getPosition().getValue()[0] << " " << pl.getPosition().getValue()[1] << " " << pl.getPosition().getValue()[2] << " ";
			//out << pl.getNormal().getValue()[0] << " " << pl.getNormal().getValue()[1] << " " << pl.getNormal().getValue()[2] << std::endl;
		}
		else if (pname == "Sphere") {
			ss >> x >> y >> z >> val;
			SpherePrimitiveShape* prim = new SpherePrimitiveShape(Sphere(Vec3f(x,y,z),val));
			prims.push_back(prim);
			prim->Release();
			count++;
		}
		else if (pname == "Cylinder") {
			ss >> x >> y >> z >> nx >> ny >> nz >> val;
			CylinderPrimitiveShape* prim = new CylinderPrimitiveShape(Cylinder(Vec3f(nx, ny, nz), Vec3f(x,y,z),val));
			prims.push_back(prim);
			prim->Release();
			count++;
		}
		else if (pname == "Cone") {
			ss >> x >> y >> z >> nx >> ny >> nz >> val;
			ConePrimitiveShape* prim = new ConePrimitiveShape(Cone(Vec3f(x, y, z), Vec3f(nx, ny, nz),val));
			prims.push_back(prim);
			prim->Release();
			count++;
		}
	}
	return count;
}