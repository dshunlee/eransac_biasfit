#pragma once

#include <set>

#include "fitheader.h"

class PrimEvaluator {
public:
	class PrimEvalMetrics {
	public:
		PrimEvalMetrics(int id=-1, int tid=-1, int n=0, int tn=0,  double per=0) :
			insID(id), tinsID(tid),tclsID(tid), clsID(id),npts(n), tnpts(tn),percent(per),
			fiterr(0), terr(0), fiterr0(0), terr0(0){}
	public:
		int tinsID; // id of matched instance
		int tclsID; // class id of matched instance
		int insID; // id of predicted instance
		int clsID; // class id of predicted instance
		double fiterr; // fitting error, using inlier
		double terr; // error on true primitive, using inlier
		double fiterr0; // using true label
		double terr0; // using true label
		int npts; // points in the correct instance label
		int tnpts; // number of total points in the instance
		double percent; // percentage of pts with correct instance label 
	};
public:
	PrimEvaluator() {}
	template<typename T1, typename T2>
	double EvalClassAccu(T1* tcls, T2* cls, std::map<std::pair<int,int>, int>& clsaccu) {// evaluate classification accuracy, tcls is the true class label
		double accu = 0;
		int neffpts = 0;
		int corrpts = 0;
		for (T1 i = -1;i < 5;i++) 
			for (T2 j = -1;j < 5;j++)
				clsaccu[std::pair<int,int>(i,j)] = 0;
		
		for (int i = 0;i < 480 * 640;i++) {
			clsaccu[std::pair<int, int>(tcls[i], cls[i])]++;
			if (tcls[i] >= 0) {
				neffpts++;
				if (tcls[i] == cls[i]) {
					corrpts++;
				}
			}
		}
		double accui = double(corrpts) / neffpts;
		//out << fi << " " << accui << " " << eps << " " << nbt << std::endl;
		//accu = accu + (accui - accu) / (fi + 1);

		return accu;
	}
	template<typename T1, typename T2>
	void findBestMatch(std::map<T1,T2> ins2cls, T1* tins, // instance to class map & true instance label for each pixel 
		PointCloud& cloud, // point cloud after fitting
		MiscLib::Vector<DetectedShape>& shapes, // predicted shapes 
		std::vector<std::vector<int> >& shapepts, // pixel id for predicted shapes
		MiscLib::Vector<MiscLib::RefCountPtr< PrimitiveShape > >& prim, // true primitives
		std::vector<PrimEvalMetrics>& pre2tru, // best match of each predicted instance, iou is used to judge wether it is a match
		double iou=0.8) { // 1 to n match from true primitive to estimated primitive

		std::set<T1> insid(tins, tins+480*640);
		std::map<T1, int> insnum; 
		std::map<T1, int> vote; 
		for (std::set<T1>::iterator it = insid.begin();it != insid.end();it++)
			insnum[*it] = 0;

		for (int i = 0;i < 480 * 640;i++) {
			insnum[tins[i]] ++;
		}
		std::map<T1,bool> hasinst;
		//std::map<int, int> pix2id;
		//for (int i = 0;i < cloud.size();i++)
		//	pix2id[cloud[i].pixID] = i;
		cloud.genPix2IDMap();

		int count = cloud.size();
		int preid = 0;
		for (MiscLib::Vector<DetectedShape>::const_iterator it = shapes.begin(); it != shapes.end(); ++it, ++preid)
		{
			for (std::set<T1>::iterator it = insid.begin();it != insid.end();it++)
				vote[*it] = 0;

			const PrimitiveShape* shape = it->first;
			unsigned shapePointsCount = static_cast<unsigned>(it->second);
			std::string desc;
			shape->Description(&desc);
			std::transform(desc.begin(), desc.end(), desc.begin(), ::tolower);
			int clas = labelmap[desc];
			

			for (unsigned j = 0; j < shapePointsCount; ++j)
				vote[tins[shapepts[preid][j]]] ++;

			PrimEvalMetrics evalmetric;
			evalmetric.insID = preid;
			evalmetric.clsID = shape->Identifier();

			std::vector<int> ptsi(shapepts[preid].size());
			for (int j = 0;j < shapepts[preid].size();j++)
				ptsi[j] = cloud.pix2id[shapepts[preid][j]];

			evalmetric.fiterr = EvalFitAccu(cloud, ptsi, shape);
			
			double perc = 0;
			for (std::map<T1, int>::iterator itv = vote.begin();itv != vote.end();itv++) {
				
				if (itv->first < 0 || itv->first >= prim.size())
					continue;

				// iou in inlier
				//perc = double(itv->second) / shapePointsCount;
				// iou in ground truth label
				perc = double(itv->second) / insnum[itv->first];
				if (shape->Identifier()==prim[itv->first]->Identifier() && perc > iou) {
					if (itv->second > evalmetric.npts) {
						evalmetric.tinsID = itv->first;
						evalmetric.tclsID = prim[itv->first]->Identifier();
						evalmetric.npts = itv->second;
						evalmetric.tnpts = insnum[itv->first];
						evalmetric.percent = perc;
					}
				}
			}
			if (evalmetric.tinsID >= 0 && evalmetric.tinsID <prim.size()) {
				
				//std::ofstream out("primi.xyz");
				//for (int i = 0;i < shapePointsCount;i++)
				//	out << cloud[ptsi[i]].pos[0] << " " << cloud[ptsi[i]].pos[1] << " " << cloud[ptsi[i]].pos[2] << std::endl;
				//out.close();
				hasinst[evalmetric.tinsID] = true;
				evalmetric.terr = EvalFitAccu(cloud, ptsi, prim[evalmetric.tinsID]);
				evalmetric.fiterr0 = EvalFitAccu(cloud, tins, shape, evalmetric.tinsID);
				evalmetric.terr0 = EvalFitAccu(cloud, tins, prim[evalmetric.tinsID], evalmetric.tinsID);
			}
			pre2tru.push_back(evalmetric);
			count -= shapePointsCount;
		}
		for (std::set<T1>::iterator it = insid.begin();it != insid.end();it++){
			if (*it>=0 && *it<prim.size() && hasinst.find(*it) == hasinst.end()) {
				PrimEvalMetrics evalmetric;
				evalmetric.insID = -1;
				evalmetric.tinsID = *it;
				evalmetric.npts = 0;
				evalmetric.tnpts = insnum[*it];
				evalmetric.percent = 0;
				evalmetric.terr = 0;
				evalmetric.tclsID = prim[*it]->Identifier();
				pre2tru.push_back(evalmetric);
			}
		}
	}
private:

	double EvalFitAccu(PointCloud& pcl, std::vector<int>& ptsid, const PrimitiveShape* tprim);//fit accuracy by project pts to true primitive
	template<typename T>
	double EvalFitAccu(PointCloud& pcl, T* tins, const PrimitiveShape* tprim, int insid){//fit accuracy by project pts to true primitive
		double err = 0;
		int count = 0;
		for(int i=0;i<pcl.size();i++)
			if (tins[pcl[i].pixID] == insid) {
				err += tprim->Distance(pcl[i].pos);
				count++;
			}
		err /= count;
		return err;
	}
};