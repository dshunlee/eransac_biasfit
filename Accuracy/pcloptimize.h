#pragma once
#include <pcl/point_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/ModelCoefficients.h>

class PCLPrimFit {
public:
	typedef pcl::PointXYZ PointT;
public:
	// return number of inliners
	int Fit(pcl::PointCloud<PointT>::Ptr cloud, pcl::PointCloud<pcl::Normal>::Ptr cloud_normals, 
		pcl::SacModel sacmod, pcl::ModelCoefficients::Ptr coefficients, pcl::PointIndices::Ptr inliers=0);
public:

};
