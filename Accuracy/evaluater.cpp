
#include "evaluater.h"

double PrimEvaluator::EvalFitAccu(PointCloud& pcl, std::vector<int>& ptsid, const PrimitiveShape* tprim) {
	double err = 0;
	for (int i = 0;i < ptsid.size();i++)
		err += tprim->Distance(pcl[ptsid[i]].pos);
	err /= ptsid.size();
	return err;
}