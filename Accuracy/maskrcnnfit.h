#pragma once

#include <math.h>
#include <vector>
#include <set>

#include "fitheader.h"
#include "primfit.h"

class MaskRcnnFit : public PrimFit {
public:
	MaskRcnnFit(double omega = 0.5) : omg(omega), shapecon(0) {}
	// overload teh loadData function
	bool loadData(std::string datfn, std::string labfn);
	// fit all of the instances
	int fit(std::vector<Options > pbfopt);
	// fit the ith instance
	bool fiti(int obji, const Options& opti);

private:
	MiscLib::Vector<size_t> find_object(int obji);
private:
	PrimitiveShapeConstructor* shapecon;
	// omega and nK for RANSAC
	double omg;
	double nK;
};