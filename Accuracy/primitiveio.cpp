
#include "primitiveio.h"
#include <fstream>

bool witePrimitives(std::string fname, MiscLib::Vector<DetectedShape>& shapes) {
	std::ofstream out(fname);
	if (out) {
		for (MiscLib::Vector<DetectedShape>::iterator it = shapes.begin(); it != shapes.end(); ++it) {
			PrimitiveShape* shape = it->first;
			std::string pname;
			shape->Description(&pname);
			out << pname << " ";
			if (pname == "Plane") {
				PlanePrimitiveShape *prim = dynamic_cast<PlanePrimitiveShape*>(shape);
				Plane pl = prim->Internal();
				out << pl.getPosition().getValue()[0] << " " << pl.getPosition().getValue()[1] << " " << pl.getPosition().getValue()[2] << " ";
				out << pl.getNormal().getValue()[0] << " " << pl.getNormal().getValue()[1] << " " << pl.getNormal().getValue()[2] << std::endl;
			}
			else if (pname == "Sphere") {
				SpherePrimitiveShape *prim = dynamic_cast<SpherePrimitiveShape*>(shape);
				Sphere sp = prim->Internal();
				out << sp.Center().getValue()[0] << " " << sp.Center().getValue()[1] << " " << sp.Center().getValue()[2] << " " << sp.Radius() << std::endl;
			}
			else if (pname == "Cylinder") {
				CylinderPrimitiveShape *prim = dynamic_cast<CylinderPrimitiveShape*>(shape);
				Cylinder cy = prim->Internal();
				Vec3f vec;
				vec = cy.AxisPosition();
				out << vec[0] << " " << vec[1] << " " << vec[2] << " ";
				vec = cy.AxisDirection();
				out << vec[0] << " " << vec[1] << " " << vec[2] << " ";
				out << cy.Radius() << std::endl;
			}
			else if (pname == "Cone") {
				ConePrimitiveShape *prim = dynamic_cast<ConePrimitiveShape*>(shape);
				Cone co = prim->Internal();
				out << co.Center().getValue()[0] << " " << co.Center().getValue()[1] << " " << co.Center().getValue()[2] << " ";
				Vec3f vec;
				vec = co.AxisDirection();
				out << vec[0] << " " << vec[1] << " " << vec[2] << " ";
				out << co.Angle() << std::endl;
			}
			else
				continue;
		}
		out.close();
		return true;
	}
	else
		return false;
}