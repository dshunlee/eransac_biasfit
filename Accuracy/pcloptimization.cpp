#include "pcloptimize.h"

int PCLPrimFit::Fit(pcl::PointCloud<PointT>::Ptr cloud, pcl::PointCloud<pcl::Normal>::Ptr cloud_normals, pcl::SacModel sacmod,
	pcl::ModelCoefficients::Ptr coefficients, pcl::PointIndices::Ptr inliers) {

	pcl::SACSegmentationFromNormals<PointT, pcl::Normal> seg;

	seg.setOptimizeCoefficients(true);
	seg.setModelType(sacmod);
	seg.setMethodType(pcl::SAC_RANSAC);
	seg.setNormalDistanceWeight(0.1);
	seg.setMaxIterations(100);
	seg.setDistanceThreshold(0.05);
	seg.setRadiusLimits(0,1);
	seg.setInputCloud(cloud);
	seg.setInputNormals(cloud_normals);

	seg.segment(*inliers, *coefficients);

	return inliers->indices.size();
}