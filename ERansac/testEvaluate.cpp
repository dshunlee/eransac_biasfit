#include "test.h"
#include "cnpy.h"
#include <string>
#include <iostream>
#include <algorithm>
#include <fstream>

#include "cnpycloud.h"
#include "evaluater.h"

#include "fitheader.h"

static void Detect(PointCloud& cloud, RansacShapeDetector::Options option, 
	MiscLib::Vector< DetectedShape >& shapes) {

	RansacShapeDetector detector(option);

	detector.Add(new PlanePrimitiveShapeConstructor());
	detector.Add(new ConePrimitiveShapeConstructor());
	detector.Add(new SpherePrimitiveShapeConstructor());
	detector.Add(new CylinderPrimitiveShapeConstructor());

	detector.Detect(cloud, 0, cloud.size(), &shapes);
}
static bool callabel(PointCloud& cloud, MiscLib::Vector< DetectedShape >& shapes, int8_t* cls) {
	if (shapes.size() <= 0) return false;
	int count = cloud.size();

	int imgsize = 480 * 640;

	if (cls == 0)
		cls = new int8_t[imgsize];

	memset(cls, -1, sizeof(int8_t) * imgsize);
	//int8_t* ins = new int8_t[imgsize];
	//memset(ins, -1, sizeof(int8_t) * imgsize);

	//std::map<std::string, int8_t> labelmap = { { "plane",0 },{ "sphere",1 },{ "cylinder",2 },{ "cone",3 } };

	for (MiscLib::Vector<DetectedShape>::const_iterator it = shapes.begin(); it != shapes.end(); ++it)
	{
		const PrimitiveShape* shape = it->first;
		unsigned shapePointsCount = static_cast<unsigned>(it->second);

		std::string desc;
		shape->Description(&desc);
		std::transform(desc.begin(), desc.end(), desc.begin(), ::tolower);

		for (unsigned j = 0; j < shapePointsCount; ++j) {
			cls[cloud[count - 1 - j].pixID] = labelmap[desc];
		}
		count -= shapePointsCount;
	}
	//save it to file

	return true;
}
static std::vector<std::string> getflist(std::string path) {
	std::vector<std::string> flist;
	std::ifstream in(path);
	while (!in.eof()) {
		std::string str;
		std::getline(in, str);
		if (!str.empty())
			flist.push_back(str);
	}

	return flist;
}

int testEvaluate(int argc, char* argv[]) {
	RansacShapeDetector::Options option(1000, 0.01, 10 * CC_DEG_TO_RAD, 10 * CC_DEG_TO_RAD, 0.05, 0.0001);

	std::vector<std::string> flist = getflist(argv[1]);

	float epsstart = atof(argv[2]);
	float epsend = atof(argv[3]);
	float deps = atof(argv[4]);
	float norstart = atof(argv[5]);
	float norend = atof(argv[6]);
	float dnor = atof(argv[7]);
	float besteps = epsstart;
	float bestnbt = norstart;
	std::string accfile(argv[8]);
	bool save = atoi(argv[9]);

#ifdef LABELINT64
	int64_t* tcls = new int64_t[480 * 640];	
#else
	int8_t* tcls = new int8_t[480 * 640];
#endif // LABELINT64

	int8_t* cls = new int8_t[480 * 640];

	double bestacc = 0;

	std::ofstream out(accfile);

	for (float eps = epsstart;eps < epsend;eps += deps) {
		for (float nbt = norstart;nbt < norend;nbt += dnor) {
			try {
				option.m_epsilon = eps;
				option.m_normalThresh = std::cos(nbt*CC_DEG_TO_RAD);
				option.m_normalThresh_glob = option.m_normalThresh;

				double accu = 0;
				std::cout << eps << " " << nbt << std::endl;
				for (int fi = 0;fi < flist.size();fi++) {
					std::cout << ".";
					PointCloud cloud;

					CloudReader::read_npz(flist[fi], cloud);
					CloudReader::loadAttr(flist[fi], tcls,"cls");

					MiscLib::Vector< DetectedShape > shapes;
					Detect(cloud, option, shapes);
					callabel(cloud, shapes, cls);

					PrimEvaluator eval;
					
					std::map<std::pair<int,int>, int> clsaccu;
					double accui = eval.EvalClassAccu(tcls,cls, clsaccu);
					out << fi << " " << accui << " " << eps << " " << nbt << std::endl;
					accu = accu + (accui - accu) / (fi + 1);

					if (save) {
						const unsigned int shape[] = { 480,640 };
						CloudReader::saveKinectImg("eransac-cls.npy", cls);
					}
				}
				std::cout << std::endl;
				out << "Av: " << accu << " " << eps << " " << nbt << std::endl;
				if (accu > bestacc) {
					bestacc = accu;
					besteps = eps;
					bestnbt = nbt;
				}
			}
			catch (std::exception& e) {
				std::cout << e.what() << std::endl;
			}
		}
	}

	out << "best" << std::endl;
	out << bestacc << " " << besteps << " " << bestnbt << std::endl;
	out.close();

	delete[] tcls;
	delete[] cls;
	return 0;

}