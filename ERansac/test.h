#pragma once

int testProbFit(int argc, char* argv[]);

int testEvaluate(int argc, char* argv[]);

int testMetrics(int argc, char* argv[]);

int testERansac(int argc, char* argv[]);

int testMaskRcnnFit(int argc, char* argv[]);

#ifdef SEGMENT_REGIONGROW
int testRegionGrow(int argc, char* argv[]);
#endif // SEGMENT_REGIONGROW


#ifdef BIASSFIT_OBJECTSEG
int testPrimRansac(int argc, char* argv[]);
#endif // BIASSFIT_OBJECTSEG



