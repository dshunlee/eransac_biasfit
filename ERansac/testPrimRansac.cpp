
#ifdef BIASSFIT_OBJECTSEG

#include "test.h"
#include "primransac.h"

#include <fstream>

#include<sstream>
#include<algorithm>
#include<vector>

#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED 
#include <boost/filesystem.hpp>

#define SIMULATED


static bool isFile(std::string path) {
	std::ifstream in(path);
	if (in)
		return true;
	else
		return false;
}


namespace fs = ::boost::filesystem;
// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
static void get_all(const fs::path& root, const std::string& ext, std::vector<fs::path>& ret)
{
	if (!fs::exists(root) || !fs::is_directory(root)) return;

	fs::recursive_directory_iterator it(root);
	fs::recursive_directory_iterator endit;

	while (it != endit)
	{
		if (fs::is_regular_file(*it) && it->path().extension() == ext) ret.push_back(it->path().filename());
		++it;

	}
	std::sort(ret.begin(), ret.end());

}

int testPrimRansac(int argc, char* argv[]) {
	std::vector<fs::path> flist;
	get_all(argv[3], ".npz", flist);

	std::vector<PrimFit::Options> pdfopt;
	readconfig(argv[1], pdfopt);

	int nopt = 6;

	double iou = 0.8;
	if (argc >= nopt) iou = atof(argv[nopt - 1]);

	int start = 0;
	int end = flist.size();
	if (argc >= nopt + 1)
		start = atoi(argv[nopt]);
	if (argc >= nopt + 2)
		end = atoi(argv[nopt + 1]);

	for (int i = start;i < end && i<flist.size();i++) {
		std::cout << flist[i].filename() << std::endl;
		std::string fname = flist[i].generic_string();
		std::string scanf = std::string(argv[2]) + "/" + fname;
		std::string resf = std::string(argv[3]) + "/" + fname;

		boost::filesystem::path dir(argv[4]);
		if (boost::filesystem::create_directory(dir))
		{
			std::cerr << "Directory Created: " << std::string(argv[4]) << std::endl;
		}

		PrimRansac fit;
		if (fit.loadData(scanf, resf)) {
			std::cout << "Fitting!" << std::endl;
			fit.fit(pdfopt);
			std::string prefix = std::string(argv[4]) + "/" + flist[i].stem().generic_string();
			std::cout << "Writting results to "<< prefix << std::endl;
			fit.calLabel();
			fit.savePrimitives(prefix);
		}
	}
	return 0;
}

#endif // BIASSFIT_OBJECTSEG