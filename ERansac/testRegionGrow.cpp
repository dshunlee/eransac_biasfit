#ifdef SEGMENT_REGIONGROW

#include "test.h"
#include "regiongrow.h"
#include<iostream>

#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED 
#include <boost/filesystem.hpp>
namespace fs = ::boost::filesystem;

// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
static void get_all(const fs::path& root, const std::string& ext, std::vector<fs::path>& ret)
{
	if (!fs::exists(root) || !fs::is_directory(root)) return;

	fs::recursive_directory_iterator it(root);
	fs::recursive_directory_iterator endit;

	while (it != endit)
	{
		if (fs::is_regular_file(*it) && it->path().extension() == ext) ret.push_back(it->path());
		++it;

	}
	std::sort(ret.begin(), ret.end());

}

int testRegionGrow(int argc, char* argv[]) {

	std::string outdir(argv[3]);
	std::vector<fs::path> flist;
	get_all(argv[2], ".npz", flist);

	std::vector<PrimFit::Options> opts;
	readconfig(argv[1], opts);

	for (int i = 0;i < flist.size();i++) {
		RegionGrow rg;
		std::string odir(outdir + "//" + flist[i].parent_path().filename().generic_string());
		boost::filesystem::path dir(odir);
		if (!fs::exists(dir))
			boost::filesystem::create_directory(dir);
		// for simulated dataset, using normal estimated by ourself is better than normal estimated by pcl
		rg.fit(flist[i].generic_string(), outdir + "//" + flist[i].parent_path().filename().generic_string() + "//" + flist[i].stem().generic_string(), opts[0],500,30,5,2,false);
	}
	
	return 0;
}

#endif