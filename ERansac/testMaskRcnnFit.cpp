
#include "test.h"

#include "maskrcnnfit.h"

#include <fstream>

#include<sstream>
#include<algorithm>

#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED 
#include "boost/filesystem.hpp"


static bool isFile(std::string path) {
	std::ifstream in(path);
	if (in)
		return true;
	else
		return false;
}


namespace fs = ::boost::filesystem;
// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
static void get_all(const fs::path& root, const std::string& ext, std::vector<fs::path>& ret)
{
	if (!fs::exists(root) || !fs::is_directory(root)) return;

	fs::recursive_directory_iterator it(root);
	fs::recursive_directory_iterator endit;

	while (it != endit){
		if (fs::is_regular_file(*it) && it->path().extension() == ext) ret.push_back(it->path().filename());
		++it;

	}
	std::sort(ret.begin(), ret.end());

}

int testMaskRcnnFit(int argc, char* argv[]) {

	if (isFile(argv[2])) {
		std::cout << "Configurations: " << std::endl;
		std::cout << "Read data in files: " << std::endl;
		std::cout << "\t" << std::string(argv[2]) << std::endl;
		std::cout << "\t" << std::string(argv[3]) << std::endl;
		std::cout << "Store result to file: " << std::endl;
		std::cout << "\t" <<  std::string(argv[4]) << std::endl;

		MaskRcnnFit fit;
		fit.loadData(argv[2], argv[3]);

		std::vector<PrimFit::Options > pdfopt;
		readconfig(argv[1], pdfopt);

		fit.fit(pdfopt);

		fit.calLabel();

		fit.savePrimitives(argv[4]);
	}
	else {
		std::cout << "Configurations: " << std::endl;
		std::cout << "Read data in folder: " << std::endl;
		std::cout << "\t" << std::string(argv[2]) << std::endl;
		std::cout << "\t" << std::string(argv[3]) << std::endl;
		std::cout << "Store result to folder: " << std::endl;
		std::cout << "\t" << std::string(argv[4]) << std::endl;

		std::vector<fs::path> flist;
		get_all(argv[3], ".npz", flist);

		std::vector<PrimFit::Options > pdfopt;
		readconfig(argv[1], pdfopt);

		int start = 0;
		int end = flist.size();

		if (argc >= 6)
			start = atoi(argv[5]);
		if (argc >= 7)
			end = atoi(argv[6]);

		for (int i = start; i < end && i<flist.size(); i++) {
			std::cout << flist[i].filename() << std::endl;
			std::string fname = flist[i].generic_string();
			std::string scanf = std::string(argv[2])  + "\\" + fname;
			std::string resf = std::string(argv[3]) + "\\" + fname;
			MaskRcnnFit fit;
			if (fit.loadData(scanf, resf)) {
				std::cout << "Fitting!" << std::endl;
				fit.fit(pdfopt);
				std::string prefix = std::string(argv[4]) + "\\" + flist[i].stem().generic_string();
				std::cout << "Writting results!" << std::endl;
				fit.calLabel(true);
				fit.savePrimitives(prefix,true);
			}
		}
	}
	return 0;
}
