#include "test.h"
#include "ProbFit.h"
#include "evaluater.h"
#include "primitiveio.h"

#include <fstream>
#include<sstream>
#include<algorithm>

#include "cnpycloud.h"

#define BOOST_FILESYSTEM_VERSION 3
#define BOOST_FILESYSTEM_NO_DEPRECATED 
#include <boost/filesystem.hpp>

#define SIMULATED


static bool isFile(std::string path) {
	std::ifstream in(path);
	if (in)
		return true;
	else
		return false;
}


namespace fs = ::boost::filesystem;
// return the filenames of all files that have the specified extension
// in the specified directory and all subdirectories
static void get_all(const fs::path& root, const std::string& ext, std::vector<fs::path>& ret)
{
	if (!fs::exists(root) || !fs::is_directory(root)) return;

	fs::recursive_directory_iterator it(root);
	fs::recursive_directory_iterator endit;

	while (it != endit)
	{
		if (fs::is_regular_file(*it) && it->path().extension() == ext) ret.push_back(it->path().filename());
		++it;

	}
	std::sort(ret.begin(), ret.end());

}


int testMetrics(int argc, char* argv[]) {

	std::vector<fs::path> flist;
	get_all(argv[3], ".npz", flist);

	std::vector<PrimFit::Options> pdfopt;
	readconfig(argv[1], pdfopt);

	int nopt = 6;

	double iou = 0.8;
	if (argc >= nopt) iou = atof(argv[nopt-1]);

	int start = 0;
	int end = flist.size();
	if (argc >= nopt+1)
		start = atoi(argv[nopt]);
	if (argc >= nopt+2)
		end = atoi(argv[nopt+1]);

#ifdef LABELINT64 // depends on the input
	int64_t* tcls = new int64_t[480 * 640];
	std::map<int64_t, int64_t> ins2cls;
#else
	int8_t* tcls = new int8_t[480 * 640];
	std::map<int8_t, int8_t> ins2cls;
#endif // LABELINT64

	
	for (int i = start;i < end && i<flist.size();i++) {
		std::cout << flist[i].filename() << std::endl;
		std::string fname = flist[i].generic_string();
		std::string scanf = std::string(argv[2]) + "/" + fname;
		std::string resf = std::string(argv[3]) + "/" + fname;

		boost::filesystem::path dir(argv[4]);
		if (boost::filesystem::create_directory(dir))
		{
			std::cerr << "Directory Created: " << std::string(argv[4]) << std::endl;
		}

#ifdef SIMULATED
		MiscLib::Vector<MiscLib::RefCountPtr< PrimitiveShape > > prims; // true shapes
		int nprims = readPrimitives(std::string(argv[2]) +"/"+ flist[i].stem().generic_string()+".prim", prims, ins2cls);
		assert(nprims > 0);
#endif
		std::vector<PrimEvaluator::PrimEvalMetrics> pre2tru;

		ProbFit fit;
		if (fit.loadData(scanf, resf)) {
			std::cout << "Fitting!" << std::endl;
			fit.fit(pdfopt,false);
			std::string prefix = std::string(argv[4]) + "/" + flist[i].stem().generic_string();
			std::cout << "Writting results!" << std::endl;
			fit.savePrimitives(prefix);

#ifdef PCLOPTIMIZATION
			std::cout << "Optimizing results!" << std::endl;
			fit.optimize();
			std::cout << "Writting optimized results!" << std::endl;
			fit.savePrimitives(prefix);
#endif

#ifdef SIMULATED
			
			PrimEvaluator eval;
			std::cout << "Load true label!" << std::endl;
			//read cls
			CloudReader::loadAttr(scanf, tcls, "cls");
			// confusion matrix
			std::map < std::pair<int, int>, int > clsaccu;
			eval.EvalClassAccu(tcls, fit.getClasPtr(),clsaccu);

			std::ofstream out1(prefix + ".conf");
			for (int ii = -1;ii < 5;ii++) {
				for (int jj = -1;jj < 5;jj++)
					out1 << clsaccu[std::pair<int, int>(ii, jj)] << " ";
				out1 << std::endl;
			}
			out1.close();

			// read ins, save it in tcls
			CloudReader::loadAttr(scanf, tcls, "ins");

			std::cout << "Finding best matches!" << std::endl;
			eval.findBestMatch(ins2cls, tcls,  fit.pcl, fit.shapes, fit.primpts,prims, pre2tru, iou);
			
			std::ofstream out2(prefix + ".pair");
			for (int i = 0;i < pre2tru.size();i++) {
				out2 << pre2tru[i].insID << " " << pre2tru[i].clsID << " " << pre2tru[i].tinsID << " " << pre2tru[i].tclsID << " ";
				out2 << pre2tru[i].npts << " " << pre2tru[i].tnpts;
				out2 << " " << pre2tru[i].fiterr << " " << pre2tru[i].terr << " " << pre2tru[i].fiterr0 << " " << pre2tru[i].terr0 << std::endl;
			}
			out2.close();
#endif
		}
	}
	return 0;
}