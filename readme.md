# ERansacBiasFit

Modified efficient ransac algorithm to fit primitives from segmented results.


1. The code requires boost library, others has been included in the package

2. Primitiveshapes and MiscLib are from efficient ransac

3. Code used for BiasFit is included in ERansac

4. set following preprocessor macros if you want to compile ERansac with certain functions:

	BIASSFIT_OBJECTSEG : this macro enables codes that segment the classification result from biasfit with boundary to connected components and apply eransac fitting for each components, it requires opencv2 and ceres;

	SEGMENT_REGIONGROW : this macro enables codes that segment the point cloud using regiongrow and then apply eransac, it requires PCL library

	PCLOPTIMIZATION : this macro enables codes that use pcl primitive fitting, it requires PCL library

5. Required libraries:

	PrimitiveShapes.lib
	
	MiscLib.lib
	
	zlibstatic.lib
	
	libboost_filesystem-vc140-mt-1_59.lib

6. Optional libraries:

	opencv_highgui300.lib
	
	opencv_imgproc300.lib
	
	opencv_core300.lib
	
	opencv_imgcodecs300.lib
	
	pcl_io_release.lib
	
	pcl_filters_release.lib
	
	pcl_segmentation_release.lib
	
	pcl_common_release.lib
	
	pcl_search_release.lib
	
	pcl_features_release.lib
	
	ceres.lib
	
7. Notice:

	The npz reader does not read compressed npz files. if the npz files are compressed, they should be uncompressed using the python script at python/uncompress.py: 
		
		uncompress.py --comp origin/folder --out output/folder

8. To run the program:
	
	check the config examples for biasfit and maskrcnn and the bat files 
	